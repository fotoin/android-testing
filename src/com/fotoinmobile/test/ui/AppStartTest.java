package com.fotoinmobile.test.ui;

import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiScrollable;
import com.android.uiautomator.core.UiSelector;
import com.fotoinmobile.test.ui.util.AppStart;
import com.fotoinmobile.test.ui.util.CheckIfUiObjectExistsBy;
import com.fotoinmobile.test.ui.util.ClickByAndWait;
import com.fotoinmobile.test.ui.util.FotoINBaseTest;
import com.fotoinmobile.test.ui.util.HelpingHand;
import com.fotoinmobile.test.ui.util.Screen;

public class AppStartTest extends FotoINBaseTest {

	public void testStartFotoIN() throws Exception {

		// Clear data, start FotoIN, check guide screens
		/* Tests Covered:
		 * 
		 * C17 0.2. First Boot
		 * 
		 */

		unlockScreen();
		AppStart.slideToUnlock();
		clearData();

		System.out.println("Start FotoIN the first time");
		AppStart.startFotoIN(getUiDevice());

		System.out.println("Checking 1st guide screen");

		CheckIfUiObjectExistsBy.text("Welcome to", "Checking welcome to text");

		CheckIfUiObjectExistsBy.text("Field Photo Documentation", "Checking Field Photo Documentation text");

		CheckIfUiObjectExistsBy.text("Please select your version:", "Checking Please select your version: text");

		final UiObject constructionButton = CheckIfUiObjectExistsBy
				.text("Construction", "Checking Construction button");
		assertTrue("Construction button property fail", constructionButton.isCheckable() && constructionButton.isChecked() && constructionButton.isClickable() && constructionButton.isEnabled() && constructionButton.isFocusable());

		final UiObject propertyButton = CheckIfUiObjectExistsBy.text("Property", "Checking Property button");
		assertTrue("Property button property fail", propertyButton.isCheckable() && !propertyButton.isChecked() && propertyButton.isClickable() && propertyButton.isEnabled() && propertyButton.isFocusable());

		final UiObject realEstateButton = CheckIfUiObjectExistsBy.text("Real Estate", "Checking Real Estate button");
		assertTrue("Real Estate button property fail", realEstateButton.isCheckable() && !realEstateButton.isChecked() && realEstateButton.isClickable() && realEstateButton.isEnabled() && realEstateButton.isFocusable());

		final UiObject getStartedButton = CheckIfUiObjectExistsBy.text("Get Started", "Checking Get Started button");
		assertTrue("Get Started button property fail", getStartedButton.isClickable() && getStartedButton.isEnabled() && getStartedButton.isFocusable());

		System.out.println("Clicking Get Started");
		getStartedButton.clickAndWaitForNewWindow();

		System.out.println("Checking 2nd guide screen");

		final UiObject navigateUpButton = CheckIfUiObjectExistsBy.descriptionRegex(".*Navigate.*",
				"Checking FotoIN button");

		CheckIfUiObjectExistsBy.text("Instructions", "Checking Instructions text");

		CheckIfUiObjectExistsBy.description("Search", "Checking Search button");

		CheckIfUiObjectExistsBy.description("Snap a Photo", "Checking Snap a Photo button");

		CheckIfUiObjectExistsBy.text("Gallery Screen Instructions", "Checking Gallery Screen Instructions text");

		CheckIfUiObjectExistsBy.text("Filter photos and view\nthem by list or by map.", "Checking Filter bubble");

		CheckIfUiObjectExistsBy.text("Search photos by\ncaptured field information.", "Checking Search bubble");

		CheckIfUiObjectExistsBy.text("Snap photos\nfrom any screen.", "Checking Snap a Photo bubble");

		CheckIfUiObjectExistsBy.text("Sync, Share, create PDF\nfiles and email photos.", "Checking Settings bubble");

		CheckIfUiObjectExistsBy.text("Tap on a photo to view it's\ninformation and comments.",
				"Checking Tap Photo bubble");

		System.out.println("Checking Gallery text mockup");
		final UiObject gallery_text_mock = new UiObject(new UiSelector().textStartsWith("Home Constructions Irmo"));
		assertTrue("Doesn't exist!", gallery_text_mock.exists());

		CheckIfUiObjectExistsBy.description("Gallery Row Instructions Image", "Checking Galery photo mockup");

		final UiObject next_button = CheckIfUiObjectExistsBy.text("Next", "Checking Next button");

		System.out.println("Clicking Next button");
		next_button.clickAndWaitForNewWindow();

		System.out.println("Checking 3rd guide screen");

		CheckIfUiObjectExistsBy.descriptionRegex(".*Navigate.*", "Checking FotoIN button");

		CheckIfUiObjectExistsBy.text("Instructions", "Checking Instructions text");

		CheckIfUiObjectExistsBy.description("Snap a Photo", "Checking Snap a Photo button");

		CheckIfUiObjectExistsBy.text("Photo Screen Instructions", "Checking Photo Screen Instructions text");

		CheckIfUiObjectExistsBy.text("Sync, email, create PDF,\nstart a Multi Photo Report\nor access Settings.",
				"Checking Settings bubble");

		CheckIfUiObjectExistsBy.text("View and edit\nPhoto Information.", "Checking View and edit text");

		CheckIfUiObjectExistsBy.description("Photo Screen Instructions Image", "Checking Photo View mockup");

		CheckIfUiObjectExistsBy.text("Hold down to move,\ndouble tap to edit, copy or delete it.",
				"Checking Annotation bubble");

		final UiObject logInButton = CheckIfUiObjectExistsBy.text("Log in", "Checking Log in button");

		System.out.println("Clicking Log in button");
		logInButton.clickAndWaitForNewWindow();

		System.out.println("Checking Log in screen");

		CheckIfUiObjectExistsBy.text("Log in", "Checking Log in text");

		final UiObject companyName = CheckIfUiObjectExistsBy.text("Company Name", "Checking Company Name field");
		assertTrue("Property fail!", companyName.isClickable() && companyName.isEnabled() && companyName.isFocusable() && companyName.isFocused());
		companyName.setText("Limun");

		System.out.println("Checking Company Password field");
		final UiObject companyPassword = new UiObject(new UiSelector().text("Limun").fromParent(new UiSelector().index(1)));
		assertTrue("Doesn't exist!", companyPassword.exists());
		assertTrue("Property fail!", companyPassword.isClickable() && companyPassword.isEnabled() && companyPassword.isFocusable());
		companyPassword.setText("123456");

		final UiObject transferTextAndCheckbox = CheckIfUiObjectExistsBy.text(
				"Transfer unassigned photos to this company", "Checking Transfer text & checkbox");
		assertTrue("Property fail!", transferTextAndCheckbox.isCheckable() && transferTextAndCheckbox.isClickable() && transferTextAndCheckbox.isEnabled()
				&& transferTextAndCheckbox.isFocusable() && !transferTextAndCheckbox.isChecked());
		System.out.println("Clicking checkbox");
		transferTextAndCheckbox.click();
		assertTrue("Property fail!", transferTextAndCheckbox.isCheckable() && transferTextAndCheckbox.isClickable() && transferTextAndCheckbox.isEnabled()
				&& transferTextAndCheckbox.isFocusable() && transferTextAndCheckbox.isChecked());

		final UiObject cancelButton = CheckIfUiObjectExistsBy.text("Cancel", "Checking Cancel button");
		assertTrue("Property fail!", cancelButton.isClickable() && cancelButton.isEnabled() && cancelButton.isFocusable());

		final UiObject okButton = CheckIfUiObjectExistsBy.text("OK", "Checking OK button");
		assertTrue("Property fail!", okButton.isClickable() && okButton.isEnabled() && okButton.isFocusable());

		System.out.println("Clicking Cancel");
		cancelButton.clickAndWaitForNewWindow();

		System.out.println("Going to Gallery");
		navigateUpButton.clickAndWaitForNewWindow();

		final UiObject galleryPhotoListView = new UiObject(new UiSelector().description("gallery_photo_list"));
		assertTrue("Not in gallery", galleryPhotoListView.exists());

		AppStart.goToHomeScreen(getUiDevice());

	}

	public void testEmptyGalleryView() throws Exception {

		//Clear data, start FotoIN, check Gallery screen
		/*
		 	Tests: covered
		    C18 0.3. Gallery View
			C20 0.3.1. Gallery View - Icons
			C19 0.3.1.1. Gallery View - Icons - View/Filter
			C21 0.3.1.4. Gallery View - Icons - Options

		 */

		unlockScreen();
		AppStart.slideToUnlock();
		clearData();
		AppStart.startFotoIN(getUiDevice());
		AppStart.skipGuide();
		gotoGalleryScreen();

		final UiObject navigateUpButton = CheckIfUiObjectExistsBy.description("FotoIN, Open drawer",
				"Checking FotoIN button");
		assertTrue("Property failed", navigateUpButton.isClickable() && navigateUpButton.isEnabled() && navigateUpButton.isFocusable());

		final UiObject searchButton = CheckIfUiObjectExistsBy.description("Search", "Checking Search button");
		assertTrue("Property failed", searchButton.isClickable() && searchButton.isEnabled() && searchButton.isFocusable() && searchButton.isLongClickable());

		final UiObject snapaPhotoButton = CheckIfUiObjectExistsBy.description("Snap a Photo",
				"Checking Snap a Photo button");
		assertTrue("Property failed", snapaPhotoButton.isClickable() && snapaPhotoButton.isEnabled() && snapaPhotoButton.isFocusable() && snapaPhotoButton.isLongClickable());

		CheckIfUiObjectExistsBy.text("Gallery > All", "Checking Gallery > All text");

		System.out.println("Opening Drawer");
		navigateUpButton.click();

		CheckIfUiObjectExistsBy.description("gallery_drawer", "Checking Drawer");

		CheckIfUiObjectExistsBy.text("Views", "Checking Views text");

		final UiObject mapText = CheckIfUiObjectExistsBy.text("Map", "Checking Map text");
		assertTrue("Not Enabled!", mapText.isEnabled());

		final UiObject fliterByText = CheckIfUiObjectExistsBy.text("Filter by:", "Checking Filter by: text");

		final UiObject projectText = CheckIfUiObjectExistsBy.text("Project", "Checking Project text");

		final UiObject addressText = CheckIfUiObjectExistsBy.text("Address", "Checking Address text");

		final UiObject deviceText = CheckIfUiObjectExistsBy.text("Device", "Checking Device text");

		final UiObject dateText = CheckIfUiObjectExistsBy.text("Date", "Checking Date text");

		final UiObject allText = CheckIfUiObjectExistsBy.text("All", "Checking All text");


		System.out.println("Clicking Project");
		projectText.click();
		assertTrue("Dosn't exist", fliterByText.exists());
		fliterByText.click();

		System.out.println("Clicking Address");
		addressText.click();
		assertTrue("Dosn't exist", fliterByText.exists());
		fliterByText.click();

		System.out.println("Clicking Device");
		deviceText.click();
		assertTrue("Dosn't exist", fliterByText.exists());
		fliterByText.click();

		System.out.println("Clicking Date");
		dateText.click();
		assertTrue("Dosn't exist", fliterByText.exists());
		fliterByText.click();

		System.out.println("Clicking All");
		allText.click();
		final UiObject galleryPhotoListView = new UiObject(new UiSelector().description("gallery_photo_list"));
		assertTrue("Not in gallery", galleryPhotoListView.exists());

		System.out.println("Checking menu buttons");

		getUiDevice().pressMenu();

		final UiObject syncAllMenuButton = CheckIfUiObjectExistsBy.text("Sync All", "Checking sync all button in menu");
		assertTrue("Not enabled!", syncAllMenuButton.isEnabled());

		final UiObject reportMenuButton = CheckIfUiObjectExistsBy.text("Start a Report",
				"Checking Start a Report button in menu");
		assertTrue("Not enabled!", reportMenuButton.isEnabled());

		final UiObject importMenuButton = CheckIfUiObjectExistsBy.text("Import a Photo",
				"Checking Import a Photo button in menu");
		assertTrue("Not enabled!", importMenuButton.isEnabled());

		final UiObject settingsMenuButton = CheckIfUiObjectExistsBy
				.text("Settings", "Checking Settings button in menu");
		assertTrue("Not enabled!", settingsMenuButton.isEnabled());

		final UiObject logInMenuButton = CheckIfUiObjectExistsBy.text("Log In", "Checking Log In in menu");
		assertTrue("Not enabled!", logInMenuButton.isEnabled());

		final UiObject feedbackMenuButton = CheckIfUiObjectExistsBy.text("Send Feedback",
				"Checking Send Feedback button in menu");
		assertTrue("Not enabled!", feedbackMenuButton.isEnabled());

		final UiObject helpMenuButton = CheckIfUiObjectExistsBy.text("Help", "Checking Help button in menu");
		assertTrue("Not enabled!", helpMenuButton.isEnabled());

		AppStart.goToHomeScreen(getUiDevice());

	}

	public void testSettingsView() throws Exception {

		// Clear data, Start FotoIN, Check Settings screen

		/*
			Tests covered:
			C52 0.3.1.4.1. Gallery View - Icons - Options - Settings
			C26 0.5. Settings
		 */
		unlockScreen();
		AppStart.slideToUnlock();
		clearData();
		AppStart.startFotoIN(getUiDevice());
		AppStart.skipGuide();

		CheckIfUiObjectExistsBy.descriptionRegex(".*.*Navigate.*", "Checking FotoIN button");

		CheckIfUiObjectExistsBy.text("Settings", "Checking Settings text");

		CheckIfUiObjectExistsBy.text("General", "Checking General text");

		final UiObject deviceNameText = CheckIfUiObjectExistsBy.text("Device Name", "Checking Device name text");
		assertTrue("Not enabled", deviceNameText.isEnabled());

		final UiObject deviceNameValue = CheckIfUiObjectExistsBy.text("Android Device", "Checking Device name value");
		assertTrue("Not enabled", deviceNameValue.isEnabled());

		final UiObject logInText = CheckIfUiObjectExistsBy.text("Log in", "Checking Log in text");
		assertTrue("Not enabled", logInText.isEnabled());

		final UiObject logInLongText = CheckIfUiObjectExistsBy.text("Log in with your FotoIN / cloud account",
				"Checking Log in long text");
		assertTrue("Not enabled", logInLongText.isEnabled());

		final UiObject logOutText = CheckIfUiObjectExistsBy.text("Log out", "Checking Log out text");
		assertTrue("Enabled!", !logOutText.isEnabled());

		final UiObject logOutLongText = CheckIfUiObjectExistsBy.text("Log out of your current FotoIN account",
				"Checking Log out long text");
		assertTrue("Enabled!", !logOutLongText.isEnabled());

		final UiObject createAccountText = CheckIfUiObjectExistsBy.text("Create a FotoIN account",
				"Checking Create a FotoIN account text");
		assertTrue("Not enabled", createAccountText.isEnabled());

		final UiObject createAccountLongText = CheckIfUiObjectExistsBy.text(
				"Create a FotoIN account on the FotoIN web site", "Checking Create a FotoIN account long text");
		assertTrue("Not enabled", createAccountLongText.isEnabled());

		CheckIfUiObjectExistsBy.text("Synchronization", "Checking Synchronization text");

		final UiObject mobileDataText = CheckIfUiObjectExistsBy.text("Mobile Network Data",
				"Checking Mobile Network Data text");
		assertTrue("Not enabled", mobileDataText.isEnabled());

		final UiObject mobileDataLongText = CheckIfUiObjectExistsBy.text(
				"Use Mobile Network Data plan when Wi-Fi is not available", "Checking Mobile Data long text");
		assertTrue("Not enabled", mobileDataLongText.isEnabled());

		final UiObject autoSyncText = CheckIfUiObjectExistsBy.text("Sync Automatically",
				"Checking Sync Automatically text");
		assertTrue("Not enabled", autoSyncText.isEnabled());

		final UiObject autoSyncLongText = CheckIfUiObjectExistsBy.text(
				"Periodically check and upload new photos and reports", "Checking Create a FotoIN account long text");
		assertTrue("Not enabled", autoSyncLongText.isEnabled());

		final UiObject photoQualityText = CheckIfUiObjectExistsBy.text("Photo Quality", "Checking Photo Quality text");
		assertTrue("Not enabled", photoQualityText.isEnabled());

		final UiObject photoQualityLongText = CheckIfUiObjectExistsBy.text(
				"Average photo quality, larger photo file size, fast sync.", "Checking Photo Quality long text");
		assertTrue("Not enabled", photoQualityLongText.isEnabled());

		System.out.println("Clicking Photo Quality");
		photoQualityText.clickAndWaitForNewWindow();

		System.out.println("Checking Photo Quality text");
		assertTrue("Doesn't exist", photoQualityText.exists());

		final UiObject bestButton = CheckIfUiObjectExistsBy.text("Best", "Checking Best text and button");
		assertTrue("Property failed", bestButton.isCheckable() && !bestButton.isChecked() && bestButton.isEnabled());

		final UiObject mediumButton = CheckIfUiObjectExistsBy.text("Medium", "Checking Medium text and button");
		assertTrue("Property failed", mediumButton.isCheckable() && mediumButton.isChecked() && mediumButton.isEnabled());

		final UiObject smallButton = CheckIfUiObjectExistsBy.text("Small", "Checking Small text and button");
		assertTrue("Property failed", smallButton.isCheckable() && !smallButton.isChecked() && smallButton.isEnabled());

		System.out.println("Clicking and checking buttons");
		bestButton.clickAndWaitForNewWindow();
		photoQualityText.clickAndWaitForNewWindow();
		assertTrue("Property of Best failed", bestButton.isCheckable() && bestButton.isChecked() && bestButton.isEnabled());
		assertTrue("Property of Medium failed", mediumButton.isCheckable() && !mediumButton.isChecked() && mediumButton.isEnabled());
		assertTrue("Property of Small failed", smallButton.isCheckable() && !smallButton.isChecked() && smallButton.isEnabled());

		smallButton.clickAndWaitForNewWindow();
		photoQualityText.clickAndWaitForNewWindow();
		assertTrue("Property of Small failed", smallButton.isCheckable() && smallButton.isChecked() && smallButton.isEnabled());
		assertTrue("Property of Best failed", bestButton.isCheckable() && !bestButton.isChecked() && bestButton.isEnabled());
		assertTrue("Property of Medium failed", mediumButton.isCheckable() && !mediumButton.isChecked() && mediumButton.isEnabled());

		mediumButton.click();
		photoQualityText.clickAndWaitForNewWindow();
		assertTrue("Property of Medium failed", mediumButton.isCheckable() && mediumButton.isChecked() && mediumButton.isEnabled());
		assertTrue("Property of Small failed", smallButton.isCheckable() && !smallButton.isChecked() && smallButton.isEnabled());
		assertTrue("Property of Best failed", bestButton.isCheckable() && !bestButton.isChecked() && bestButton.isEnabled());

		final UiObject cancelButton = CheckIfUiObjectExistsBy.text("Cancel", "Checking Cancel button");
		assertTrue("Property failed", cancelButton.isClickable() && cancelButton.isEnabled() && cancelButton.isFocusable());

		System.out.println("Clicking Cancel button");
		cancelButton.clickAndWaitForNewWindow();

		System.out.println("Scrolling down");
		final UiScrollable listView = new UiScrollable(new UiSelector().className("android.widget.ListView"));
		listView.scrollForward();

		CheckIfUiObjectExistsBy.text("Application Information", "Checking Application Information text");

		final UiObject versionText = CheckIfUiObjectExistsBy.text("Version", "Checking Version text");
		assertTrue("Not enabled!", versionText.isEnabled());

		// VERZIJA!!!!

		final UiObject versionValue = CheckIfUiObjectExistsBy.text("2.9.0", "Checking Version value"); // Varijabla za verziju?!?
		assertTrue("Not enabled!", versionValue.isEnabled());

		final UiObject locationText = CheckIfUiObjectExistsBy.text("Current location (touch to refresh)",
				"Checking Location text");
		assertTrue("Not enabled!", locationText.isEnabled());

		final UiObject deviceIdText = CheckIfUiObjectExistsBy.text("Device ID", "Checking Device ID text");
		assertTrue("Not enabled!", deviceIdText.isEnabled());

		//Device ID?

		final UiObject copyrightText = CheckIfUiObjectExistsBy.text("Copyright", "Checking Copyright text");
		assertTrue("Not enabled!", copyrightText.isEnabled());

		final UiObject fotoinMobileText = CheckIfUiObjectExistsBy.text("FotoIN Mobile Corporation",
				"Checking FotoIN Mobile Corporation text");
		assertTrue("Not enabled!", fotoinMobileText.isEnabled());

		final UiObject errorReportText = CheckIfUiObjectExistsBy.text("Send Error Report",
				"Checking Send Error Report text");
		assertTrue("Not enabled!", errorReportText.isEnabled());

		final UiObject errorReportLongText = CheckIfUiObjectExistsBy.text(
				"Send error report to help us improve FotoIN Mobile", "Checking Send Error Report long text");
		assertTrue("Not enabled!", errorReportLongText.isEnabled());

		//Opening enviroment button
		Screen.showEnviromentButton();

		final UiObject environmentText = CheckIfUiObjectExistsBy.text("Environment", "Checking Environment text");
		assertTrue("Not enabled!", environmentText.isEnabled());

		final UiObject productionValue = CheckIfUiObjectExistsBy.text("PRODUCTION",
				"Checking Environment value and clicking");
		assertTrue("Not enabled!", productionValue.isEnabled());
		environmentText.click();



		final UiObject testValue = new UiObject(new UiSelector().text("TEST"));
		assertTrue("Doesn't exist", testValue.exists());
		assertTrue("Not enabled!", testValue.isEnabled());

		environmentText.click();
		final UiObject devValue = new UiObject(new UiSelector().text("DEV"));
		assertTrue("Doesn't exist", devValue.exists());
		assertTrue("Not enabled!", devValue.isEnabled());

		environmentText.click();
		assertTrue("Doesn't exist", productionValue.exists());
		assertTrue("Not enabled!", productionValue.isEnabled());

		AppStart.goToHomeScreen(getUiDevice());

	}

	public void testLogInLogOut() throws Exception {

		// Clear data, Start FotoIN, Log in to company, sync, check photo, log out, check log in/out buttons
		/*
			Tests covered:
			C27 0.5.1. Settings - Login
			C29 0.5.2. Settings - Logout
			C852 0.5.3.1. Device Name after logging into Company
			C33 0.5.5. Settings - Debug Menu

		 */
		unlockScreen();
		AppStart.slideToUnlock();
		clearData();
		AppStart.startFotoIN(getUiDevice());
		AppStart.skipGuide();
		companyLogin("uitesting", "123456");
		AppStart.autoSyncOnOff(0);

		Thread.sleep(1000);

		final UiObject deviceNameText = CheckIfUiObjectExistsBy.text("Device Name", "Checking Device name text");
		assertTrue("Enabled", !deviceNameText.isEnabled());

		System.out.println("Checking Device name value");
		final UiObject deviceNameValueLogedIn = new UiObject(new UiSelector().text("Registered as \"Android Device\""));
		assertTrue("Doesn't exist", deviceNameValueLogedIn.exists());
		assertTrue("Enabled", !deviceNameValueLogedIn.isEnabled());

		final UiObject logInText = CheckIfUiObjectExistsBy.text("Log in", "Checking Log in text");
		assertTrue("Enabled", !logInText.isEnabled());

		System.out.println("Checking Log in long text");
		final UiObject logInLongTextLogedIn = new UiObject(new UiSelector().textContains("uitesting"));
		assertTrue("Doesn't exist", logInLongTextLogedIn.exists());
		assertTrue("Enabled", !logInLongTextLogedIn.isEnabled());

		final UiObject logOutText = CheckIfUiObjectExistsBy.text("Log out", "Checking Log out text");
		assertTrue("Not Enabled!", logOutText.isEnabled());

		final UiObject logOutLongText = CheckIfUiObjectExistsBy.text("Log out of your current FotoIN account",
				"Checking Log out long text");
		assertTrue("Not Enabled!", logOutLongText.isEnabled());

		gotoGalleryScreen();

		sync();

		System.out.println("Checking Photo name");
		final UiObject photoNameGallery = new UiObject(new UiSelector().textContains("Petrovaradinska"));
		assertTrue("Doesn't exist", photoNameGallery.exists());
		assertTrue("Not Enagled!", photoNameGallery.isEnabled());

		final UiObject photoThumbGallery = CheckIfUiObjectExistsBy.description("Title", "Checking photo thumb");
		assertTrue("Not enabled", photoThumbGallery.isEnabled());

		getUiDevice().pressMenu();

		ClickByAndWait.text("Start a Report", "Opening reports");

		final UiObject basicReport = CheckIfUiObjectExistsBy.text("Basic report", "Checking Basic report");
		final UiObject customReportRich = CheckIfUiObjectExistsBy.text("Demo & Test Rich",
				"Checking Demo & Test Rich report");
		final UiObject customReport = CheckIfUiObjectExistsBy.text("Demo & Test", "Checking Demo & Test report");

		ClickByAndWait.description("Start a Report, Navigate up", "Returning to Gallery");

		getUiDevice().pressMenu();

		ClickByAndWait.text("Settings", "Opening Settings");

		ClickByAndWait.text("Log out", "Logging out");

		CheckIfUiObjectExistsBy.text("Device Name", "Checking Device name text");
		assertTrue("Not enabled", deviceNameText.isEnabled());

		final UiObject deviceNameValueLogedOut = CheckIfUiObjectExistsBy.text("Android Device",
				"Checking Device name value");
		assertTrue("Not enabled", deviceNameValueLogedOut.isEnabled());

		CheckIfUiObjectExistsBy.text("Log in", "Checking Log in text");
		assertTrue("Not enabled", logInText.isEnabled());

		final UiObject logInLongTextLogedOut = CheckIfUiObjectExistsBy.text("Log in with your FotoIN / cloud account",
				"Checking Log in long text");
		assertTrue("Not enabled", logInLongTextLogedOut.isEnabled());

		CheckIfUiObjectExistsBy.text("Log out", "Checking Log out text");
		assertTrue("Enabled!", !logOutText.isEnabled());

		CheckIfUiObjectExistsBy.text("Log out of your current FotoIN account", "Checking Log out long text");
		assertTrue("Enabled!", !logOutLongText.isEnabled());

		gotoGalleryScreen();

		System.out.println("Checking gallery");
		assertTrue("There's a photo!", !photoNameGallery.exists());
		assertTrue("Thumbnail exists", !photoThumbGallery.exists());

		System.out.println("Checking reports");
		getUiDevice().pressMenu();
		ClickByAndWait.text("Start a Report", "Opening reports");
		assertTrue("Basic report missing", basicReport.exists());
		assertTrue("Custom report(s) present", !customReport.exists() || !customReportRich.exists());

		ClickByAndWait.description("Start a Report, Navigate up", "Returning to gallery");

		HelpingHand.snapPhoto();

		Screen.openDrawer(getUiDevice());

		final UiObject projectValue = CheckIfUiObjectExistsBy.resourceId(
				"com.fotoinmobile.construction:id/photo_meta_account", "Getting Project value");
		final UiObject addressValue = CheckIfUiObjectExistsBy.resourceId(
				"com.fotoinmobile.construction:id/photo_meta_location", "Getting Address value");

		assertEquals("Address and Project name fail", projectValue.getText(), addressValue.getText());

		CheckIfUiObjectExistsBy.text("Android Device", "Checking Device name");

		System.out.println("Checking 2nd Custom Tag field");
		final UiObject customTag2 = new UiObject(new UiSelector().text("Test 2"));
		assertTrue("2nd Custom Tag exists", !customTag2.exists());

		System.out.println("Checking 1st Custom Tag field");
		final UiObject customTag1 = new UiObject(new UiSelector().text("Test 1"));
		assertTrue("1st Custom Tag field exists", !customTag1.exists());

		ClickByAndWait.description("Photo View, Navigate up", "Returning to Gallery");

	}

	public void testPhotoView() throws Exception {

		// Starting FotoIN, Logging into company, syncing, checking photo view and drawer
		/*
		 * C22 0.4. Photo View
		 * C38 0.4.1. Photo View - Icons
		 * C39 0.4.1.1. Photo View - Icons - Gallery View
		 * C42 0.4.1.3. Photo View - Icons - Options
		 * C55 0.4.1.4.1. Photo View - Icons - Options - Settings
		 * C4604	1.6.1.1. Photo tagging - Construction - Default Master Tag
		 * C14	1.6.4. Photo tagging - Custom tags
		 * 
		 */

		unlockScreen();
		AppStart.slideToUnlock();
		clearData();
		AppStart.startFotoIN(getUiDevice());
		AppStart.skipGuide();
		companyLogin("uitesting", "123456");
		AppStart.autoSyncOnOff(0);
		gotoGalleryScreen();

		sync();

		ClickByAndWait.description("Title", "Downloading photo");
		HelpingHand.waitForPhotoDownload();

		final UiObject fotoinButton = CheckIfUiObjectExistsBy.description("FotoIN, Navigate up",
				"Checking FotoIN button");
		assertTrue("Property Fail", fotoinButton.isClickable() && fotoinButton.isEnabled() && fotoinButton.isFocusable());

		CheckIfUiObjectExistsBy.text("Photo View", "Checking Photo View text");

		final UiObject snapaPhotoButton = CheckIfUiObjectExistsBy.description("Snap a Photo",
				"Checking Snap a Photo Button");
		assertTrue("Property Fail", snapaPhotoButton.isClickable() && snapaPhotoButton.isEnabled() && snapaPhotoButton.isFocusable());

		final UiObject openDrawer = CheckIfUiObjectExistsBy.description("Open drawer", "Checking Drawer");
		assertTrue("Property Fail", openDrawer.isClickable() && openDrawer.isEnabled() && openDrawer.isFocusable());

		System.out.println("Checking Menu items");

		getUiDevice().pressMenu();

		final UiObject syncAllButton = CheckIfUiObjectExistsBy.text("Sync All", "Checking Sync All button");
		assertTrue("Not Enabled", syncAllButton.isEnabled());

		final UiObject pdfButton = CheckIfUiObjectExistsBy.text("Export to PDF", "Checking Export to PDF button");
		assertTrue("Not Enabled", pdfButton.isEnabled());

		final UiObject emailPhotoButton = CheckIfUiObjectExistsBy.text("E-mail photo", "Checking E-mail photo Button");
		assertTrue("Not Enabled", emailPhotoButton.isEnabled());

		final UiObject settingsButton = CheckIfUiObjectExistsBy.text("Settings", "Checking Settings button");
		assertTrue("Not Enabled", settingsButton.isEnabled());

		final UiObject feedbackButton = CheckIfUiObjectExistsBy.text("Send Feedback", "Checking Send Feedback button");
		assertTrue("Not Enabled", feedbackButton.isEnabled());

		final UiObject helpButton = CheckIfUiObjectExistsBy.text("Help", "Checking Help button");
		assertTrue("Not Enabled", helpButton.isEnabled());

		System.out.println("Closing Menu");
		getUiDevice().pressBack();

		Screen.openDrawer(getUiDevice());

		CheckIfUiObjectExistsBy.text("Photo Info", "Checking Photo Info text");

		final UiObject editButton = CheckIfUiObjectExistsBy.text("Edit", "Checking Edit Button");
		assertTrue("Property fail", editButton.isClickable() && editButton.isEnabled() && editButton.isFocusable());

		CheckIfUiObjectExistsBy.text("Title", "Checking Title text");

		System.out.println("Checking Filename");
		final UiObject filenameText = new UiObject(new UiSelector().textStartsWith("Novar"));
		assertTrue("Doesn't exist", filenameText.exists());

		// CheckIfUiObjectExistsBy.Text("Nova", "Checking Filename");

		CheckIfUiObjectExistsBy.text("Project", "Checking Project text");

		CheckIfUiObjectExistsBy.text("Novar", "Checking Project value");

		CheckIfUiObjectExistsBy.text("Device", "Checking Device text");

		CheckIfUiObjectExistsBy.text("Nexus 10", "Checking Device value");

		CheckIfUiObjectExistsBy.text("Address & Date", "Checking Address & Date text");

		CheckIfUiObjectExistsBy.text("Petrovaradinska ulica 3, Zagreb", "Checking Address value");

		CheckIfUiObjectExistsBy.resourceId("com.fotoinmobile.construction:id/photo_meta_location_coords",
				"Checking GPS value");

		CheckIfUiObjectExistsBy.resourceId("com.fotoinmobile.construction:id/photo_meta_date",
				"Checking Date & Time value");

		CheckIfUiObjectExistsBy.text("Tags & Comments", "Checking Tags & Comments text");

		CheckIfUiObjectExistsBy.description("Tag label master", "Checking Const Trade Tag text");

		CheckIfUiObjectExistsBy.text("General", "Checking Cons Tag value");

		CheckIfUiObjectExistsBy.text("General Comments", "Checking General Comments text");

		CheckIfUiObjectExistsBy.resourceId("com.fotoinmobile.construction:id/photo_meta_comments",
				"Checking General Comments field");

		ClickByAndWait.text("Edit", "Clicking Edit button");

		CheckIfUiObjectExistsBy.text("Done", "Checking Done button");

		final UiObject projectField = CheckIfUiObjectExistsBy.text("Novar", "Checking Project value");
		assertTrue("Property fail", projectField.isClickable() && projectField.isEnabled() && projectField.isFocusable() && projectField.isLongClickable());

		final UiObject addressField = CheckIfUiObjectExistsBy.text("Petrovaradinska ulica 3, Zagreb",
				"Checking Address value");
		assertTrue("Property fail", addressField.isClickable() && addressField.isEnabled() && addressField.isFocusable() && addressField.isLongClickable());

		final UiObject findAddressButton = CheckIfUiObjectExistsBy.description("Find adresss_button",
				"Checking Find Address Button");
		assertTrue("Property fail", findAddressButton.isClickable() && findAddressButton.isEnabled() && findAddressButton.isFocusable());

		final UiObject primaryTagField = CheckIfUiObjectExistsBy.description("Tag button master",
				"Checking Const Tag field");
		assertTrue("Property fail",
				primaryTagField.isClickable() && primaryTagField.isEnabled() && primaryTagField.isFocusable());

		CheckIfUiObjectExistsBy.text("Test 1", "Cehecking 2nd Custom Tag text");

		System.out.println("Checking 1st Custom Tag field");
		final UiObject customTag1 = new UiObject(new UiSelector().description("Tag button Test 1"));
		assertTrue("Doesn't exist", customTag1.exists());
		assertTrue("Property fail", customTag1.isClickable() && customTag1.isEnabled() && customTag1.isFocusable());

		System.out.println("Checking 2ndst Custom Tag field");
		final UiObject customTag2 = new UiObject(new UiSelector().description("Tag button Test 2"));
		assertTrue("Doesn't exist", customTag2.exists());
		assertTrue("Property fail", customTag2.isClickable() && customTag2.isEnabled() && customTag2.isFocusable());

		//TEST General Comments field... Description missing.

		//Checking Construction tags

		ClickByAndWait.description("Tag button master", "Opening Const Trade Tags");

		final UiObject generalTag = CheckIfUiObjectExistsBy.text("General", "Checking General tag value");
		assertTrue("Property Fail", generalTag.isCheckable() && generalTag.isEnabled());

		final UiObject concretelTag = CheckIfUiObjectExistsBy.text("Concrete", "Checking Concrete tag value");
		assertTrue("Property Fail", concretelTag.isCheckable() && concretelTag.isEnabled());

		final UiObject masonryTag = CheckIfUiObjectExistsBy.text("Masonry", "Checking Masonry tag value");
		assertTrue("Property Fail", masonryTag.isCheckable() && masonryTag.isEnabled());

		final UiObject metalsTag = CheckIfUiObjectExistsBy.text("Metals", "Checking Metals tag value");
		assertTrue("Property Fail", metalsTag.isCheckable() && metalsTag.isEnabled());

		final UiObject woodAndPlasticsTag = CheckIfUiObjectExistsBy.text("Wood and Plastics",
				"Checking Wood and Plastics tag value");
		assertTrue("Property Fail", woodAndPlasticsTag.isCheckable() && woodAndPlasticsTag.isEnabled());

		final UiObject thermalAndMoistureTag = CheckIfUiObjectExistsBy.text("Thermal and Moisture",
				"Checking Thermal and Moisture tag value");
		assertTrue("Property Fail", thermalAndMoistureTag.isCheckable() && thermalAndMoistureTag.isEnabled());

		final UiObject doorsAndFramesTag = CheckIfUiObjectExistsBy.text("Doors and Frames",
				"Checking Doors and Frames tag value");
		assertTrue("Property Fail", doorsAndFramesTag.isCheckable() && doorsAndFramesTag.isEnabled());

		final UiScrollable listView = new UiScrollable(new UiSelector().scrollable(true));
		listView.scrollTextIntoView("Finishes");
		final UiObject finishesTag = CheckIfUiObjectExistsBy.text("Finishes", "Checking Finishes tag value");
		assertTrue("Property Fail", finishesTag.isCheckable() && finishesTag.isEnabled());

		listView.scrollTextIntoView("Specialties");
		final UiObject specialtiesTag = CheckIfUiObjectExistsBy.text("Specialties", "Checking Specialties tag value");
		assertTrue("Property Fail", specialtiesTag.isCheckable() && specialtiesTag.isEnabled());

		listView.scrollTextIntoView("Equipment");
		final UiObject equipmentTag = CheckIfUiObjectExistsBy.text("Equipment", "Checking Equipment tag value");
		assertTrue("Property Fail", equipmentTag.isCheckable() && equipmentTag.isEnabled());

		listView.scrollTextIntoView("Furnishings");
		final UiObject furnishingsTag = CheckIfUiObjectExistsBy.text("Furnishings", "Checking Furnishings tag value");
		assertTrue("Property Fail", furnishingsTag.isCheckable() && furnishingsTag.isEnabled());

		listView.scrollTextIntoView("Special Construction");
		final UiObject specialConstructionTag = CheckIfUiObjectExistsBy.text("Special Construction",
				"Checking Special Construction tag value");
		assertTrue("Property Fail", specialConstructionTag.isCheckable() && specialConstructionTag.isEnabled());

		listView.scrollTextIntoView("Conveying Systems");
		final UiObject conveyingSystemsTag = CheckIfUiObjectExistsBy.text("Conveying Systems",
				"Checking Conveying Systems tag value");
		assertTrue("Property Fail", conveyingSystemsTag.isCheckable() && conveyingSystemsTag.isEnabled());

		listView.scrollTextIntoView("Mechanical");
		final UiObject mechanicalTag = CheckIfUiObjectExistsBy.text("Mechanical", "Checking Mechanical tag value");
		assertTrue("Property Fail", mechanicalTag.isCheckable() && mechanicalTag.isEnabled());

		listView.scrollTextIntoView("Electrical");
		final UiObject electricalTag = CheckIfUiObjectExistsBy.text("Electrical", "Checking Electrical tag value");
		assertTrue("Property Fail", electricalTag.isCheckable() && electricalTag.isEnabled());

		listView.scrollTextIntoView("Exterior");
		final UiObject exteriorTag = CheckIfUiObjectExistsBy.text("Exterior", "Checking Exterior tag value");
		assertTrue("Property Fail", exteriorTag.isCheckable() && exteriorTag.isEnabled());

		listView.scrollTextIntoView("Windows");
		final UiObject windowsTag = CheckIfUiObjectExistsBy.text("Windows", "Checking Windows tag value");
		assertTrue("Property Fail", windowsTag.isCheckable() && windowsTag.isEnabled());

		listView.scrollTextIntoView("Flooring");
		final UiObject flooringTag = CheckIfUiObjectExistsBy.text("Flooring", "Checking Flooring tag value");
		assertTrue("Property Fail", flooringTag.isCheckable() && flooringTag.isEnabled());

		listView.scrollTextIntoView("Plumbing");
		final UiObject plumbingTag = CheckIfUiObjectExistsBy.text("Plumbing", "Checking Plumbing tag value");
		assertTrue("Property Fail", plumbingTag.isCheckable() && plumbingTag.isEnabled());

		System.out.println("Test C4604	1.6.1.1. Photo tagging - Construction - Default Master Tag  passed");

		listView.scrollTextIntoView("Metals");
		ClickByAndWait.text("Metals", "Clicking Metals");

		ClickByAndWait.text("OK", "Clicking OK");

		System.out.println("Opening 1st custom tag list");
		customTag1.clickAndWaitForNewWindow();

		final UiObject noneTag = CheckIfUiObjectExistsBy.text("None", "Checking None tag");
		assertTrue("Property Fail", noneTag.isCheckable() && noneTag.isEnabled());

		final UiObject alphaTag = CheckIfUiObjectExistsBy.text("Alpha", "Checking Alpha tag");
		assertTrue("Property Fail", alphaTag.isCheckable() && alphaTag.isEnabled());

		final UiObject betaTag = CheckIfUiObjectExistsBy.text("Beta", "Checking Beta tag");
		assertTrue("Property Fail", betaTag.isCheckable() && betaTag.isEnabled());

		final UiObject charlieTag = CheckIfUiObjectExistsBy.text("Charlie", "Checking Charlie tag");
		assertTrue("Property Fail", charlieTag.isCheckable() && charlieTag.isEnabled());

		final UiObject deltaTag = CheckIfUiObjectExistsBy.text("Delta", "Checking Delta tag");
		assertTrue("Property Fail", deltaTag.isCheckable() && deltaTag.isEnabled());

		final UiObject deselectButton = CheckIfUiObjectExistsBy.text("Deselect", "Checking Deselect button");
		assertTrue("Property Fail", deselectButton.isClickable() && deselectButton.isEnabled());

		ClickByAndWait.text("Charlie", "Clicking Charlie");

		ClickByAndWait.text("OK", "Clicking OK");

		System.out.println("Opening 1st custom tag list");
		customTag2.clickAndWaitForNewWindow();

		CheckIfUiObjectExistsBy.text("None", "Checking None tag");
		assertTrue("Property Fail", noneTag.isCheckable() && noneTag.isEnabled());

		final UiObject golfTag = CheckIfUiObjectExistsBy.text("Golf", "Checking Golf tag");
		assertTrue("Property Fail", golfTag.isCheckable() && golfTag.isEnabled());

		final UiObject hotelTag = CheckIfUiObjectExistsBy.text("Hotel", "Checking Hotel tag");
		assertTrue("Property Fail", hotelTag.isCheckable() && hotelTag.isEnabled());

		final UiObject echoTag = CheckIfUiObjectExistsBy.text("Echo", "Checking Echo tag");
		assertTrue("Property Fail", echoTag.isCheckable() && echoTag.isEnabled());

		final UiObject foxtrotTag = CheckIfUiObjectExistsBy.text("Foxtrot", "Checking Foxtrot tag");
		assertTrue("Property Fail", foxtrotTag.isCheckable() && foxtrotTag.isEnabled());

		ClickByAndWait.text("Foxtrot", "Clicking Foxtrot");

		ClickByAndWait.text("OK", "Clicking OK");

		ClickByAndWait.text("Done", "Clicking Done button");
		fotoinButton.clickAndWaitForNewWindow();
		ClickByAndWait.description("Title", "Opening Photo view");
		Screen.openDrawer(getUiDevice());

		CheckIfUiObjectExistsBy.text("Metals", "Checking Const Trade Tag value");
		CheckIfUiObjectExistsBy.text("Foxtrot", "Checking 2nd custom Tag value");
		CheckIfUiObjectExistsBy.text("Charlie", "Checking 1st custom Tag value");

		System.out.println("Test C14	1.6.4. Photo tagging - Custom tags passed");

		//Cleanup

		editButton.clickAndWaitForNewWindow();

		ClickByAndWait.description("Tag button master", "");
		;
		listView.scrollTextIntoView("General");
		ClickByAndWait.text("General", "");

		ClickByAndWait.text("OK", "");

		customTag1.clickAndWaitForNewWindow();

		deselectButton.clickAndWaitForNewWindow();

		ClickByAndWait.text("OK", "");

		customTag2.clickAndWaitForNewWindow();

		deselectButton.clickAndWaitForNewWindow();

		ClickByAndWait.text("OK", "");

		ClickByAndWait.text("Done", "Clicking Done button");

		fotoinButton.clickAndWaitForNewWindow();

		AppStart.goToHomeScreen(getUiDevice());

	}

	public void testPhotoDataAndFilters() throws Exception {

		//
		/*
		 * C2 1.1.Photo Capture
		 * C1958 1.10.1. Edit Address
		 * C46 1.9. Edit Project/Listing/Account name
		 * C41 0.4.1.2. Photo View - Icons - Photo
		 * C59 0.3.1.1.2.1. Gallery View - Icons - View/Filter - Filter by Project/Listing/Account
		 * C67 0.3.1.1.2.2. Gallery View - Icons - View/Filter - Filter by Address
		 * C68 0.3.1.1.2.3. Gallery View - Icons - View/Filter - Filter by Device
		 * C70 0.3.1.1.2.5. Gallery View - Icons - View/Filter - Filter by All
		 * C60 0.3.1.1.1.2. Gallery View - Icons - View/Filter - List function
		 * C71 0.3.1.1.3.1. Map View - Icons - View/Filter - Filter by Project/Listing/Account
		 * C58 0.3.1.1.1.1. Gallery View - Icons - View/Filter - Map function
		 * C72 0.3.1.1.3.2. Map View - Icons - View/Filter - Filter by Address
		 * C73 0.3.1.1.3.3. Map View - Icons - View/Filter - Filter by Device
		 * C75 0.3.1.1.3.5. Map View - Icons - View/Filter - Filter by All
		 * C37 0.3.1.3. Gallery View - Icons - Photo
		 * 
		 * C69 0.3.1.1.2.4. Gallery View - Icons - View/Filter - Filter by Date
		 * C74 0.3.1.1.3.4. Map View - Icons - View/Filter - Filter by Date
		 * 
		 * C28 0.5.3. Settings - Device Name
		 * 
		 * C36	0.3.1.2. Gallery View - Icons - Search
		 * C4109	0.3.1.2. Gallery View - Icons - Multiple Word Search
		 */

		unlockScreen();
		AppStart.slideToUnlock();
		clearData();
		AppStart.startFotoIN(getUiDevice());
		AppStart.skipGuide();
		gotoGalleryScreen();

		HelpingHand.snapPhoto();

		System.out.println("Test C41 0.4.1.2. Photo View - Icons - Photo passed");

		Screen.openDrawer(getUiDevice());

		ClickByAndWait.description("Edit_button", "Clicking Edit");

		// Editing project name

		System.out.println("Editing Project name");
		final UiObject projectValue = new UiObject(new UiSelector().resourceId("com.fotoinmobile.construction:id/photo_meta_account"));
		projectValue.setText("Project Alpha");

		//Editing address

		System.out.println("Editing Address");
		final UiObject addressValue = new UiObject(new UiSelector().resourceId("com.fotoinmobile.construction:id/photo_meta_location"));
		addressValue.setText("Address Alpha");

		CheckIfUiObjectExistsBy.text("Android Device", "Checking device name");

		ClickByAndWait.text("Done", "Clicking Done");

		Screen.closeGalleryDrawer();

		//Adding annotation

		final UiObject imageView = new UiObject(new UiSelector().description("Tag a Photo"));
		imageView.click();

		CheckIfUiObjectExistsBy.text("Annotation:", "Checking Annotation: text");

		final UiObject annotationText = CheckIfUiObjectExistsBy.resourceId(
				"com.fotoinmobile.construction:id/annotation_edit_text", "Checking Annotation textbox");
		annotationText.setText("Annotation Alpha");

		CheckIfUiObjectExistsBy.text("Cancel", "Checking Cancel button");
		CheckIfUiObjectExistsBy.text("OK", "Checking OK button");
		ClickByAndWait.text("OK", "Clicking OK button");

		//Returning to gallery

		ClickByAndWait.description("FotoIN, Navigate up", "Returning to Gallery");

		final UiObject alphaPhoto = CheckIfUiObjectExistsBy.textRegex(
				"Project.*Alpha_Address.*Alpha_Android.*Device.*", "Checking photo in Gallery");

		System.out.println("C2 1.1.Photo Capture Passed");

		//Going to settings

		getUiDevice().pressMenu();
		ClickByAndWait.text("Settings", "Opening Settings");

		//Changing device name

		ClickByAndWait.text("Device Name", "Changeing Device name");
		final UiObject deviceNameText = new UiObject(new UiSelector().resourceId("android:id/edit"));
		deviceNameText.setText("Another ");
		ClickByAndWait.text("OK", "Clicking OK");

		//Returning to gallery

		gotoGalleryScreen();

		//Adding another photo

		HelpingHand.snapPhoto();

		Screen.openDrawer(getUiDevice());

		ClickByAndWait.description("Edit_button", "Clicking Edit");

		// Checking device name

		CheckIfUiObjectExistsBy.textRegex("Another.*Device", "Checking device name");

		//Editing project name

		System.out.println("Editing Project name");
		projectValue.setText("Project Beta");

		//Editing address

		System.out.println("Editing Address");
		addressValue.setText("Address Beta");
		getUiDevice().pressBack();

		ClickByAndWait.text("Done", "Clicking Done");
		Screen.closeGalleryDrawer();

		//Adding annotation

		imageView.click();
		annotationText.setText("Annotation Beta");
		ClickByAndWait.text("OK", "Clicking OK button");

		//Returning to gallery

		gotoGalleryScreen();

		//Adding another photo

		HelpingHand.snapPhoto();

		Screen.openDrawer(getUiDevice());

		ClickByAndWait.description("Edit_button", "Clicking Edit");

		//Editing project name

		System.out.println("Editing Project name");
		projectValue.setText("Project Charlie");

		//Editing address

		System.out.println("Editing Address");
		addressValue.setText("Address Charlie");

		ClickByAndWait.text("Done", "Clicking Done");
		Screen.closeGalleryDrawer();

		//Adding annotation

		imageView.click();
		annotationText.setText("Annotation Charlie");
		ClickByAndWait.text("OK", "Clicking OK button");

		//Returning to gallery

		gotoGalleryScreen();

		//Checking photos in gallery

		final UiObject betaPhoto = new UiObject(
				new UiSelector().textMatches("Project.*Beta_Address.*Beta_Another.*Device.*"));
		final UiObject charliePhoto = CheckIfUiObjectExistsBy.textRegex(
				"Project.*Charlie_Address.*Charlie_Another.*Device.*", "Checking photo in Gallery");
		betaPhoto.clickAndWaitForNewWindow();

		Screen.openDrawer(getUiDevice());

		//Checking Beta photo

		CheckIfUiObjectExistsBy.text("Project Beta", "Checking Project name");
		System.out.println("Test C46 1.9. Edit Project/Listing/Account name passed");
		CheckIfUiObjectExistsBy.text("Address Beta", "Checking Address name");
		System.out.println("Test C1958 1.10.1. Edit Address passed");

		CheckIfUiObjectExistsBy.textRegex("Another.*Device", "Checking Device name");
		System.out.println("Test C28 0.5.3. Settings - Device Name passed");


		ClickByAndWait.description("Photo View, Navigate up", "Returning to Gallery");

		//Checking List View

		ClickByAndWait.description("FotoIN, Open drawer", "Opening Drawer");

		//Checking Project

		ClickByAndWait.text("Project", "Clicking Project");

		CheckIfUiObjectExistsBy.text("Project Alpha", "Checking Project Alpha");

		CheckIfUiObjectExistsBy.text("Project Beta", "Checking Project Beta");

		ClickByAndWait.text("Project Alpha", "Clicking Project Alpha");
		assertTrue("Photo Alpha doesn't exist", alphaPhoto.exists());
		assertTrue("Photo Beta exists", !betaPhoto.exists());
		CheckIfUiObjectExistsBy.text("Gallery > Project > Project Alpha",
				"Checking Gallery text \"Gallery > Project > Project Alpha\"");

		ClickByAndWait.description("FotoIN, Open drawer", "Opening drawer");

		ClickByAndWait.text("Project Beta", "Clicking Project Beta");
		assertTrue("Photo Beta doesn't exist", betaPhoto.exists());
		assertTrue("Photo Alpha exists", !alphaPhoto.exists());
		CheckIfUiObjectExistsBy.text("Gallery > Project > Project Beta",
				"Checking Gallery text \"Gallery > Project > Project Beta\"");

		ClickByAndWait.description("FotoIN, Open drawer", "Opening drawer");

		ClickByAndWait.text("Filter by:", "Clicking Filter by");

		//Checking Address

		ClickByAndWait.text("Address", "Clicking Address");

		CheckIfUiObjectExistsBy.text("Address Alpha", "Checking Address Alpha");

		CheckIfUiObjectExistsBy.text("Address Beta", "Checking Address Beta");

		ClickByAndWait.text("Address Alpha", "Clicking Address Alpha");
		assertTrue("Photo Alpha doesn't exist", alphaPhoto.exists());
		assertTrue("Photo Beta exists", !betaPhoto.exists());
		CheckIfUiObjectExistsBy.text("Gallery > Address > Address Alpha",
				"Checking Gallery text \"Gallery > Address > Address Alpha\"");

		ClickByAndWait.description("FotoIN, Open drawer", "Opening drawer");

		ClickByAndWait.text("Address Beta", "Clicking Address Beta");
		assertTrue("Photo Beta doesn't exist", betaPhoto.exists());
		assertTrue("Photo Alpha exists", !alphaPhoto.exists());
		CheckIfUiObjectExistsBy.text("Gallery > Address > Address Beta",
				"Checking Gallery text \"Gallery > Address > Address Beta\"");

		ClickByAndWait.description("FotoIN, Open drawer", "Opening drawer");

		ClickByAndWait.text("Filter by:", "Clicking Filter by");

		//Checking Device

		ClickByAndWait.text("Device", "Clicking Device");

		CheckIfUiObjectExistsBy.text("Android Device", "Checking Android Device");

		CheckIfUiObjectExistsBy.textRegex("Another.*Device", "Checking Android Device 2");

		ClickByAndWait.text("Android Device", "Clicking Android Device");
		assertTrue("Photo Alpha doesn't exist", alphaPhoto.exists());
		assertTrue("Photo Beta exists", !betaPhoto.exists());
		CheckIfUiObjectExistsBy.text("Gallery > Device > Android Device",
				"Checking Gallery text \"Gallery > Device > Android Device\"");

		ClickByAndWait.description("FotoIN, Open drawer", "Opening drawer");

		ClickByAndWait.textRegex("Another.*Device", "Clicking Another Android Device");
		assertTrue("Photo Beta doesn't exist", betaPhoto.exists());
		assertTrue("Photo Alpha exists", !alphaPhoto.exists());
		CheckIfUiObjectExistsBy.text("Gallery > Device > Another Android Device",
				"Checking Gallery text \"Gallery > Device > Another Android Device\"");

		ClickByAndWait.description("FotoIN, Open drawer", "Opening drawer");

		ClickByAndWait.text("Filter by:", "Clicking Filter by");

		// Checking Date

		ClickByAndWait.text("Date", "Clicking Date");

		CheckIfUiObjectExistsBy.text(HelpingHand.isoDate(), "Checking " + HelpingHand.isoDate());

		ClickByAndWait.text(HelpingHand.isoDate(), "Clicking " + HelpingHand.isoDate());
		assertTrue("Photo Alpha doesn't exist", alphaPhoto.exists());
		assertTrue("Photo Beta doesn't exist", betaPhoto.exists());
		CheckIfUiObjectExistsBy.text("Gallery > Date > " + HelpingHand.isoDate(),
				"Checking Gallery text \"Gallery > Date > " + HelpingHand.isoDate() + "\"");

		ClickByAndWait.description("FotoIN, Open drawer", "Opening drawer");

		ClickByAndWait.text("Filter by:", "Clicking Filter by");

		//Checking All

		ClickByAndWait.text("All", "Clicking All");
		assertTrue("Photo Alpha doesn't exist", alphaPhoto.exists());
		assertTrue("Photo Beta doesn't exist", betaPhoto.exists());
		CheckIfUiObjectExistsBy.text("Gallery > All", "Checking Gallery text \"Gallery > All\"");

		ClickByAndWait.description("FotoIN, Open drawer", "Opening drawer");

		//Checking Map View

		ClickByAndWait.text("Map", "Clicking Map");
		CheckIfUiObjectExistsBy.text("List", "Checking List button");


		//Checking All

		ClickByAndWait.text("All", "Clicking All");

		final UiObject alphaPhotoMap = CheckIfUiObjectExistsBy.descriptionRegex(
				".*Project\\s*Alpha_Address\\s*Alpha.*", "Checking Project Alpha");
		final UiObject betaPhotoMap = CheckIfUiObjectExistsBy.descriptionRegex(".*Project\\s*Beta_Address\\s*Beta.*",
				"Checking Project Beta");
		CheckIfUiObjectExistsBy.text("Gallery > All", "Checking Gallery text \"Gallery > All\"");

		ClickByAndWait.description("FotoIN, Open drawer", "Opening drawer");

		//Checking Project

		ClickByAndWait.text("Project", "Clicking Project");

		CheckIfUiObjectExistsBy.text("Project Alpha", "Checking Project Alpha");

		CheckIfUiObjectExistsBy.text("Project Beta", "Checking Project Beta");

		ClickByAndWait.text("Project Alpha", "Clicking Project Alpha");
		assertTrue("Photo Alpha doesn't exist", alphaPhotoMap.exists());
		assertTrue("Photo Beta exists", !betaPhotoMap.exists());
		CheckIfUiObjectExistsBy.text("Gallery > Project > Project Alpha",
				"Checking Gallery text \"Gallery > Project > Project Alpha\"");

		ClickByAndWait.description("FotoIN, Open drawer", "Opening drawer");

		ClickByAndWait.text("Project Beta", "Clicking Project Beta");
		assertTrue("Photo Beta doesn't exist", betaPhotoMap.exists());
		assertTrue("Photo Alpha exists", !alphaPhotoMap.exists());
		CheckIfUiObjectExistsBy.text("Gallery > Project > Project Beta",
				"Checking Gallery text \"Gallery > Project > Project Beta\"");

		ClickByAndWait.description("FotoIN, Open drawer", "Opening drawer");

		ClickByAndWait.text("Filter by:", "Clicking Filter by");

		//Checking Address

		ClickByAndWait.text("Address", "Clicking Address");

		CheckIfUiObjectExistsBy.text("Address Alpha", "Checking Address Alpha");

		CheckIfUiObjectExistsBy.text("Address Beta", "Checking Address Beta");

		ClickByAndWait.text("Address Alpha", "Clicking Address Alpha");
		assertTrue("Photo Alpha doesn't exist", alphaPhotoMap.exists());
		assertTrue("Photo Beta exists", !betaPhotoMap.exists());
		CheckIfUiObjectExistsBy.text("Gallery > Address > Address Alpha",
				"Checking Gallery text \"Gallery > Address > Address Alpha\"");

		ClickByAndWait.description("FotoIN, Open drawer", "Opening drawer");

		ClickByAndWait.text("Address Beta", "Clicking Address Beta");
		assertTrue("Photo Beta doesn't exist", betaPhotoMap.exists());
		assertTrue("Photo Alpha exists", !alphaPhotoMap.exists());
		CheckIfUiObjectExistsBy.text("Gallery > Address > Address Beta",
				"Checking Gallery text \"Gallery > Address > Address Beta\"");

		ClickByAndWait.description("FotoIN, Open drawer", "Opening drawer");

		ClickByAndWait.text("Filter by:", "Clicking Filter by");

		//Checking Device

		ClickByAndWait.text("Device", "Clicking Device");

		CheckIfUiObjectExistsBy.text("Android Device", "Checking Android Device");

		CheckIfUiObjectExistsBy.text("Another Android Device", "Checking Another Android Device");

		ClickByAndWait.text("Android Device", "Clicking Android Device");
		assertTrue("Photo Alpha doesn't exist", alphaPhotoMap.exists());
		assertTrue("Photo Beta exists", !betaPhotoMap.exists());
		CheckIfUiObjectExistsBy.text("Gallery > Device > Android Device",
				"Checking Gallery text \"Gallery > Device > Android Device\"");

		ClickByAndWait.description("FotoIN, Open drawer", "Opening drawer");

		ClickByAndWait.text("Another Android Device", "Another Android Device");
		assertTrue("Photo Beta doesn't exist", betaPhotoMap.exists());
		assertTrue("Photo Alpha exists", !alphaPhotoMap.exists());
		CheckIfUiObjectExistsBy.text("Gallery > Device > Another Android Device",
				"Checking Gallery text \"Gallery > Device > Another Android Device\"");

		ClickByAndWait.description("FotoIN, Open drawer", "Opening drawer");

		ClickByAndWait.text("Filter by:", "Clicking Filter by");

		// Checking Date

		ClickByAndWait.text("Date", "Clicking Date");

		CheckIfUiObjectExistsBy.text(HelpingHand.isoDate(), "Checking " + HelpingHand.isoDate());

		ClickByAndWait.text(HelpingHand.isoDate(), "Clicking " + HelpingHand.isoDate());
		assertTrue("Photo Alpha doesn't exist", alphaPhotoMap.exists());
		assertTrue("Photo Beta doesn't exist", betaPhotoMap.exists());
		CheckIfUiObjectExistsBy.text("Gallery > Date > " + HelpingHand.isoDate(),
				"Checking Gallery text \"Gallery > Date > " + HelpingHand.isoDate() + "\"");

		ClickByAndWait.description("FotoIN, Open drawer", "Opening drawer");

		ClickByAndWait.text("Filter by:", "Clicking Filter by");

		ClickByAndWait.text("List", "Clicking List");

		CheckIfUiObjectExistsBy.text("Map", "Checking Map button");

		ClickByAndWait.text("All", "Clicking All");

		//Checking Search

		ClickByAndWait.description("Search", "Clicking search");

		final UiObject searchBox = CheckIfUiObjectExistsBy.description("Search query", "Checking Search box");
		searchBox.setText("Alpha");
		getUiDevice().pressEnter();

		CheckIfUiObjectExistsBy.textRegex("Gallery > Search > Alpha", "Checking \"Gallery > Search > Alpha\" text");
		CheckIfUiObjectExistsBy.textRegex("Project.*Alpha_Address.*Alpha_Android.*Device.*",
				"Checking photo in Gallery");

		System.out.println("Test C36	0.3.1.2. Gallery View - Icons - Search Passed");

		ClickByAndWait.description("FotoIN, Navigate up", "Returning to gallery");

		ClickByAndWait.description("Search", "Clicking search");

		searchBox.setText("Alpha Beta");
		getUiDevice().pressEnter();

		CheckIfUiObjectExistsBy
		.text("Gallery > Search > Alpha Beta", "Checking \"Gallery > Search > Alpha Beta\" text");
		assertTrue("Search failed", alphaPhoto.exists() && betaPhoto.exists() && !charliePhoto.exists());

		System.out.println("Test C4109	0.3.1.2. Gallery View - Icons - Multiple Word Search  Passed");
	}

	public void testREVersion() throws Exception {
		/*
		 * C4605	1.6.3.1. Photo tagging - Real Estate - Default Master Tag
		 * C99	2.2. Basic Report - Real estate
		 */


		clearData();
		AppStart.slideToUnlock();
		AppStart.startFotoIN(getUiDevice());
		ClickByAndWait.text("Real Estate", "Choosing RE version");
		AppStart.skipGuide();
		gotoGalleryScreen();
		HelpingHand.snapPhoto();

		Screen.openDrawer(getUiDevice());
		CheckIfUiObjectExistsBy.text("Rent/Sale Tag", "Checking Master tag text");
		ClickByAndWait.text("None", "Opening Master tag");
		CheckIfUiObjectExistsBy.text("None", "Checking tag value None");
		CheckIfUiObjectExistsBy.text("Sale", "Checking tag value Sale");
		CheckIfUiObjectExistsBy.text("Rent/Sale", "Checking tag value Rent/Sale");
		ClickByAndWait.text("None", "Clicking None");
		ClickByAndWait.description("Done_button", "Clicking Done_button");
		ClickByAndWait.description("Photo View, Navigate up", "Returning to gallery");

		System.out.println("Test Photo tagging - Real Estate - Default Master Tag passed");

		System.out.println("Starting RE Basic report");

		ClickByAndWait.description("More options", "Clicking More options");
		ClickByAndWait.text("Start a Report", "Clicking Start a Report");
		ClickByAndWait.text("Basic report", "Clicking Basic report");

		CheckIfUiObjectExistsBy.textRegex("Report Information", "Checking Report Information text");
		CheckIfUiObjectExistsBy.text("Title", "Checking Title text");
		CheckIfUiObjectExistsBy.text("Address", "Checking Address text");
		CheckIfUiObjectExistsBy.text("Date", "Checking Date text");
		CheckIfUiObjectExistsBy.text("Device", "Checking Device text");

		final UiObject forRent = CheckIfUiObjectExistsBy.text("For Rent", "Checking For Rent checkbox");
		assertTrue("Properties fail", forRent.isCheckable() && !forRent.isChecked() && forRent.isClickable() && forRent.isEnabled());
		final UiObject forSale = CheckIfUiObjectExistsBy.text("For Sale", "Checking For Sale checkbox");
		assertTrue("Properties fail", forSale.isCheckable() && !forSale.isChecked() && forSale.isClickable() && forSale.isEnabled());

		CheckIfUiObjectExistsBy.text("Beds", "Checking Beds text");
		final UiObject bedsValue = CheckIfUiObjectExistsBy.resourceId(
				"com.fotoinmobile.construction:id/report_beds_number", "Checking Beds value");
		assertEquals(bedsValue.getText(), "0");
		CheckIfUiObjectExistsBy.text("Baths", "Checking Baths text");
		final UiObject bathsValue = CheckIfUiObjectExistsBy.resourceId(
				"com.fotoinmobile.construction:id/report_baths_number", "Checking Baths value");
		assertEquals(bathsValue.getText(), "0.0");

		CheckIfUiObjectExistsBy.text("Home Space (sq ft)", "Checking Home space text");
		final UiObject homeSpaceValue = new UiObject(new UiSelector().resourceId("com.fotoinmobile.construction:id/report_home_space"));
		homeSpaceValue.setText("1337");

		final UiObject lotSizeValue = new UiObject(new UiSelector().resourceId("com.fotoinmobile.construction:id/report_lot_size"));
		lotSizeValue.setText("1000");

		final UiObject yearBuild = new UiObject(new UiSelector().resourceId("com.fotoinmobile.construction:id/report_year_build"));
		yearBuild.setText("1977");

		getUiDevice().pressBack();

		CheckIfUiObjectExistsBy.text("Lot Size (sq ft)", "Checking Lot siize teyt");
		CheckIfUiObjectExistsBy.text("Year Build", "Checking Year Build text");
		CheckIfUiObjectExistsBy.textRegex(".*Report.*Summary.*", "Checking Report Summary text");
		CheckIfUiObjectExistsBy.text("Conclusions", "Checking Conclusions text");
		final UiObject conclusionsValue = new UiObject(new UiSelector().resourceId("com.fotoinmobile.construction:id/report_conclusions"));
		conclusionsValue.setText("Star Wars");

		final UiScrollable reportScroll = new UiScrollable(new UiSelector().resourceId("com.fotoinmobile.construction:id/report_scroll_container"));
		reportScroll.scrollIntoView(new UiSelector().text("Action Steps"));
		CheckIfUiObjectExistsBy.text("Action Steps", "Checking Action Steps text");

		getUiDevice().pressBack();

		final UiObject actionStepsValue = new UiObject(new UiSelector().resourceId("com.fotoinmobile.construction:id/report_action_steps"));
		actionStepsValue.setText("Han Solo");

		getUiDevice().pressBack();

		forRent.click();
		forSale.click();
		assertTrue("Properties fail", forRent.isCheckable() && forRent.isChecked() && forRent.isClickable() && forRent.isEnabled());
		assertTrue("Properties fail", forSale.isCheckable() && forSale.isChecked() && forSale.isClickable() && forSale.isEnabled());

		ClickByAndWait.description("More options", "Clicking More options");
		CheckIfUiObjectExistsBy.text("Reset", "Checking Reset option");
		CheckIfUiObjectExistsBy.text("Preview", "Checking Preview option");
		CheckIfUiObjectExistsBy.text("Done", "Checking Done option");
		CheckIfUiObjectExistsBy.text("Settings", "Checking Settings option");
		CheckIfUiObjectExistsBy.text("Log In", "Checking Log In option");
		CheckIfUiObjectExistsBy.text("Send Feedback", "Checking Send Feedback option");
		CheckIfUiObjectExistsBy.text("Help", "Checking Reset option");
		ClickByAndWait.text("Reset", "Reseting report");

		CheckIfUiObjectExistsBy.text("Do you want to reset form?", "Checking Yes/No popup");
		CheckIfUiObjectExistsBy.text("No", "Checking No button");
		CheckIfUiObjectExistsBy.text("Yes", "Checking Yes button");
		ClickByAndWait.text("Yes", "Clicking Yes");

		System.out.println("Checking checkboxes");
		assertTrue("For Rent properties fail", forRent.isCheckable() && !forRent.isChecked() && forRent.isClickable() && forRent.isEnabled());
		assertTrue("For Sale properties fail", forSale.isCheckable() && !forSale.isChecked() && forSale.isClickable() && forSale.isEnabled());

		assertEquals(homeSpaceValue.getText(), "Enter Home Space (sq ft) here..");
		assertEquals(lotSizeValue.getText(), "Enter Lot Size (sq ft) here..");
		assertEquals(yearBuild.getText(), "Enter Year Build here..");
		assertEquals(conclusionsValue.getText(), "Enter Conclusion here..");
		reportScroll.scrollTextIntoView("Action Steps");
		assertEquals(actionStepsValue.getText(), "Enter Action Steps here..");

	}

	public void testTransferPhoto() throws Exception {

		/*
		 * 
		 * C116	0.5.1.1. Settings - Login and transfer photos to company
		 * 
		 */

		AppStart.slideToUnlock();
		clearData();
		AppStart.startFotoIN(getUiDevice());
		AppStart.skipGuide();
		AppStart.autoSyncOnOff(0);
		companyLogin("uitesting", "123456");
		gotoGalleryScreen();
		sync();
		CheckIfUiObjectExistsBy.textRegex("Novar_Petrovaradinska.*ulica.*3,.*Zagreb_Nexus.*10.*",
				"Checking synced photo");

		gotoSettingsScreen();
		ClickByAndWait.text("Log out", "Loging out");

		gotoGalleryScreen();
		HelpingHand.snapPhoto();
		Screen.openDrawer(getUiDevice());

		final UiObject projectValue = new UiObject(new UiSelector().resourceId("com.fotoinmobile.construction:id/photo_meta_account"));
		projectValue.setText("Import fotke");
		ClickByAndWait.description("Done_button", "Clicking Done");
		ClickByAndWait.description("Photo View, Navigate up", "Returning to gallery");
		gotoSettingsScreen();

		final UiObject newPhoto = new UiObject(new UiSelector().resourceId("com.fotoinmobile.construction:id/row_title"));

		ClickByAndWait.text("Log in", "Clicking Log in");

		System.out.println("Entering credentials");
		final UiObject compamyName = new UiObject(new UiSelector().text("Company Name"));
		compamyName.setText("uitesting");
		final UiObject companyPassword = new UiObject(new UiSelector().text("uitesting").fromParent(new UiSelector().index(1)));
		companyPassword.setText("123456");

		ClickByAndWait.resourceId("com.fotoinmobile.construction:id/copy_photos", "Clicking Transfer checkbox");

		ClickByAndWait.text("OK", "Clicking OK");
		getUiDevice().waitForIdle();

		gotoGalleryScreen();

		CheckIfUiObjectExistsBy.textRegex("Novar_Petrovaradinska.*ulica.*3,.*Zagreb_Nexus.*10.*",
				"Checking synced photo");
		CheckIfUiObjectExistsBy.text(newPhoto.getText(), "Checking transfered photo");


	}

	public void testAnnotations() throws Exception {

		/*
		 * 
		 * C9	1.5. Photo annotation
		 * C10	1.5.1. Photo Annotation - Multiple Annotations
		 * C79	1.5.2. Photo annotation - Moving Annotations
		 * C80	1.5.3. Photo annotation - Edit Annotation
		 * C81	1.5.4. Photo Annotation - Delete Annotation
		 * C96	1.5.5. Photo Annotation - Copy Annotation
		 * 
		 */

		unlockScreen();
		AppStart.slideToUnlock();
		clearData();
		AppStart.startFotoIN(getUiDevice());
		AppStart.skipGuide();
		gotoGalleryScreen();

		//Taking a photo

		HelpingHand.snapPhoto();
		final UiObject imageView = new UiObject(new UiSelector().description("Tag a Photo"));
		imageView.click();

		//Setting an annotation

		final UiObject annotationText = new UiObject(new UiSelector().resourceId("com.fotoinmobile.construction:id/annotation_edit_text"));
		annotationText.setText("Text");
		ClickByAndWait.text("OK", "Clicking OK");

		gotoGalleryScreen();

		ClickByAndWait.resourceId("com.fotoinmobile.construction:id/row_title", "Opening photo");

		final UiObject annotationFirst = CheckIfUiObjectExistsBy.text("Text", "Checking annotation");

		annotationFirst.click();
		annotationFirst.click();

		CheckIfUiObjectExistsBy.text("Edit", "Checking Edit button");
		CheckIfUiObjectExistsBy.text("Delete", "Checking Delete button");
		CheckIfUiObjectExistsBy.text("Copy", "Checking Copy button");

		ClickByAndWait.text("Edit", "Clicking Edit button");
		annotationText.clearTextField();
		annotationText.setText("Edited text");
		ClickByAndWait.text("OK", "Clicking OK");

		gotoGalleryScreen();

		ClickByAndWait.resourceId("com.fotoinmobile.construction:id/row_title", "Opening photo");

		final UiObject annotationEdit = CheckIfUiObjectExistsBy.text("Edited text", "Checking annotation");

		annotationEdit.click();
		annotationEdit.click();

		ClickByAndWait.text("Copy", "Clicking Copy");

		gotoGalleryScreen();
		ClickByAndWait.resourceId("com.fotoinmobile.construction:id/row_title", "Opening photo");

		annotationEdit.click();
		annotationEdit.click();

		ClickByAndWait.text("Edit", "Clicking Edit button");
		annotationText.clearTextField();
		annotationText.setText("Double Edited");
		ClickByAndWait.text("OK", "Clicking OK");

		gotoGalleryScreen();
		ClickByAndWait.resourceId("com.fotoinmobile.construction:id/row_title", "Opening photo");

		CheckIfUiObjectExistsBy.textRegex("Edited.*text", "Checking first annotation");
		CheckIfUiObjectExistsBy.textRegex("Double.*Edited.*text", "Checking second annotation");

		final UiObject secondAnnotation = new UiObject(new UiSelector().textMatches("Double.*Edited.*text"));

		secondAnnotation.click();
		secondAnnotation.click();

		ClickByAndWait.text("Delete", "Deleteing second annotation");

		gotoGalleryScreen();
		ClickByAndWait.resourceId("com.fotoinmobile.construction:id/row_title", "Opening photo");

		CheckIfUiObjectExistsBy.textRegex("Edited.*text.*", "Checking first annotation");
		assertTrue("Second annotation exists", !secondAnnotation.exists());

		annotationEdit.dragTo(680, 550, 100);

		gotoGalleryScreen();
		ClickByAndWait.resourceId("com.fotoinmobile.construction:id/row_title", "Opening photo");

		assertTrue("Annotation not on right coordinates", annotationEdit.getBounds().centerX() < 685 &&  annotationEdit.getBounds().centerX() > 675  && annotationEdit.getBounds().centerY() < 555 && annotationEdit.getBounds().centerY() > 545);



	}

	public void testPropertyTags() throws Exception {

		/*
		 * 
		 * C43	1.6.2. Photo tagging - Property
		 * 
		 */

		unlockScreen();
		AppStart.slideToUnlock();
		clearData();
		AppStart.startFotoIN(getUiDevice());

		ClickByAndWait.text("Property", "Choosing property version");

		AppStart.skipGuide();
		AppStart.autoSyncOnOff(0);
		gotoGalleryScreen();
		HelpingHand.snapPhoto();

		Screen.openDrawer(getUiDevice());

		final UiObject defaultTags = new UiObject(new UiSelector().description("Const tag_text"));

		if (defaultTags.exists()){
			System.out.println("Default Tags exist");
			fail();
		}

		gotoGalleryScreen();
		gotoSettingsScreen();

		companyLogin("uitesting property", "123456");

		gotoGalleryScreen();

		sync();

		HelpingHand.snapPhoto();

		Screen.openDrawer(getUiDevice());

		if (defaultTags.exists()){
			System.out.println("Default Tags exist");
			fail();
		}

		CheckIfUiObjectExistsBy.text("Custom group", "Checking Custom group tags");

		ClickByAndWait.description("Tag button Custom group", "Opening custom tags");

		CheckIfUiObjectExistsBy.text("Custom Alpha", "Checking Custom Alpha tag");

		CheckIfUiObjectExistsBy.text("Custom Beta", "Checking Custom Beta tag");

		CheckIfUiObjectExistsBy.text("Custom Charlie", "Checking Custom Charlie tag");

		CheckIfUiObjectExistsBy.text("None", "Checking None tag");

		ClickByAndWait.text("Cancel", "Clicking Cancel");

		getUiDevice().pressBack();

	}

	public void testDeletePhoto() throws Exception {

		/*
		 *  C847	1.12. Delete Photo from device
		 *  C856	3.2.7. Gallery View - Do not download images deleted from the device
		 * 
		 */

		unlockScreen();
		AppStart.slideToUnlock();
		clearData();
		AppStart.startFotoIN(getUiDevice());
		AppStart.skipGuide();
		AppStart.autoSyncOnOff(0);
		companyLogin("uitesting", "123456");
		gotoGalleryScreen();
		sync();

		final UiObject photoToDelete = new UiObject(new UiSelector().resourceId("com.fotoinmobile.construction:id/row_image"));
		longPress(photoToDelete);
		CheckIfUiObjectExistsBy.description("Delete", "Checking delete button");
		CheckIfUiObjectExistsBy.text("Done", "Checking Done button");

		ClickByAndWait.text("Done", "Clicking Done button");

		assertTrue("Photo is not in gallery", photoToDelete.exists());

		longPress(photoToDelete);

		ClickByAndWait.description("Delete", "Clicking Delete");

		assertTrue("Photo is in gallery", photoToDelete.exists());

		sync();

		assertTrue("Photo is in gallery", photoToDelete.exists());

	}

	public void testSyncMetadata() throws Exception {

		//		C15		3.2. Gallery View - Photo Sync
		//		C431	3.2.1. Gallery View - Annotation Sync
		//		C432	3.2.2. Gallery View - Comment Sync
		//		C433	3.2.3. Gallery View - Tag Sync
		//		C434	3.2.4. Gallery View - Address and Coordinates Sync
		//		C435	3.2.5. Gallery View - Project/Listing/Account Sync
		//		C853	3.2.8. Download image from Cloud Service
		//				Testing sync through all providers

		unlockScreen();
		AppStart.slideToUnlock();
		clearData();
		AppStart.startFotoIN(getUiDevice());
		AppStart.skipGuide();
		AppStart.autoSyncOnOff(0);

		syncTest("uitesting box", "123456", "Box");
		syncTest("uitesting sf", "123456", "Sharefile");
		syncTest("uitesting e", "123456", "Egnyte");
		syncTest("uitesting webdav", "123456", "WebDAV");



	}

	public void testImportEXIFphoto() throws Exception {

		/*
		 * 
		 * C1694	1.4.3. Import Photo from device with EXIF data
		 * C7	1.4. Import Photo from device
		 * 
		 */

		unlockScreen();
		AppStart.slideToUnlock();
		clearData();
		AppStart.startFotoIN(getUiDevice());
		AppStart.skipGuide();
		AppStart.autoSyncOnOff(0);
		gotoGalleryScreen();

		ClickByAndWait.description("More options", "Clicking More options");
		ClickByAndWait.text("Import a Photo", "Clicking Import a photo");

		final UiObject photoWithGPS = new UiObject(new UiSelector().className("android.view.View").instance(3));
		photoWithGPS.clickAndWaitForNewWindow();

		Screen.openDrawer(getUiDevice());

		CheckIfUiObjectExistsBy.textRegex(".*N.*45.*13.*56.*E.*13.*36.*4.*", "Checking GPS coordinates.");

		gotoGalleryScreen();
		HelpingHand.snapPhoto();
		Screen.openDrawer(getUiDevice());

		final String project = HelpingHand.randomString();
		final String address = HelpingHand.randomString();

		final UiObject projectValue = new UiObject(new UiSelector().description("Project_value"));
		projectValue.setText(project);

		final UiObject addressValue = new UiObject(new UiSelector().description("Address_value"));
		addressValue.setText(address);
		ClickByAndWait.text("Done", "Clicking Done");

		gotoGalleryScreen();

		ClickByAndWait.description("More options", "Clicking More options");
		ClickByAndWait.text("Import a Photo", "Clicking Import a photo");

		final UiObject photoWithoutGPS = new UiObject(new UiSelector().className("android.view.View").instance(2));
		photoWithoutGPS.clickAndWaitForNewWindow();

		Screen.openDrawer(getUiDevice());
		ClickByAndWait.text("Done", "Clicking Done");
		final UiObject newProjectValue = new UiObject(new UiSelector().description("Project_value_view_mode"));
		assertEquals("Wrong project value", project, newProjectValue.getText());


	}

	public void testProjectPicker() throws Exception {

		/*
		 * C82	1.9.1. Edit project name - Project/Listing/Account switching
		 * C1834	3.7.1. Synchronization and Web Application Projects - Entering the radius of a Web defined project
		 * C1835	3.7.2. Synchronization and Web Application Projects - Within radius of two intersecting projects
		 * C3701	3.8. Synchronize with a Company that has a invalid logo file
		 */

		unlockScreen();
		AppStart.slideToUnlock();
		clearData();
		AppStart.startFotoIN(getUiDevice());
		AppStart.skipGuide();
		companyLogin("uitesting", "123456");
		AppStart.autoSyncOnOff(0);
		gotoGalleryScreen();
		sync();

		HelpingHand.snapPhoto();

		Screen.openDrawer(getUiDevice());

		ClickByAndWait.description("Edit_button", "Clicking Edit bitton");

		final UiObject projectValue = new UiObject(new UiSelector().description("Project_value"));

		if (projectValue.getText() == "Unknown")
			ClickByAndWait.description("Find adresss_button", "Refreshing address");

		assertEquals("Wrong project value - Not Test1", "Test1", projectValue.getText());

		ClickByAndWait.description("Project_picker", "Changing project");
		assertEquals("Wrong project value - Not Test2", "Test2", projectValue.getText());

		ClickByAndWait.description("Project_picker", "Changing project");
		assertEquals("Wrong project value - Not Novar", "Not Novar", projectValue.getText());



	}

	public void testArchivedProject() throws Exception {

		/*
		 * 
		 *  C1953	4.1. Archiving Projects - Projects Synced on Mobile device
		 *  C2304   4.2. Archiving Projects - Projects Not Synced on Mobile device
		 *  C2305	4.3. Archiving Projects - Upload Projects beloging to an Archived Project
		 * 
		 * 
		 */

		unlockScreen();
		AppStart.slideToUnlock();
		clearData();
		AppStart.startFotoIN(getUiDevice());
		AppStart.skipGuide();
		AppStart.autoSyncOnOff(0);

		final UiScrollable listView = new UiScrollable(new UiSelector().className("android.widget.ListView"));
		listView.scrollToEnd(3);

		Screen.showEnviromentButton();

		AppStart.changeEnvironmentTo("TEST");

		listView.scrollToBeginning(5);
		companyLogin("uitesting", "123456");
		gotoGalleryScreen();
		sync();

		CheckIfUiObjectExistsBy.textRegex("Novar_Petrovaradinska.*ulica.*3,.*Zagreb_Nexus.*10_2014-08-08",
				"Checking Novar photo");

		final UiObject newPhoto = new UiObject(new UiSelector().textMatches("Archived_Firmat_Nexus.*"));
		assertTrue("Archived photo exists", !newPhoto.exists());

		HelpingHand.snapPhoto();

		Screen.openDrawer(getUiDevice());

		ClickByAndWait.description("Edit_button", "Clicking Edit button");

		final UiObject projectValue = new UiObject(new UiSelector().description("Project_value"));
		projectValue.setText("Archived");
		ClickByAndWait.text("Done", "Clicking Done");

		gotoGalleryScreen();

		assertTrue("Archived photo doesn't exists", newPhoto.exists());

		Thread.sleep(31000);

		sync();

		AppStart.goToHomeScreen(getUiDevice());

		clearData();
		AppStart.startFotoIN(getUiDevice());
		AppStart.skipGuide();
		AppStart.autoSyncOnOff(0);

		listView.scrollToEnd(3);

		Screen.showEnviromentButton();

		AppStart.changeEnvironmentTo("TEST");

		listView.scrollToBeginning(5);
		companyLogin("uitesting", "123456");
		gotoGalleryScreen();
		sync();

		CheckIfUiObjectExistsBy.textRegex("Novar_Ulica.*Pile.*I.*4.*Zagreb_Nexus.*Test.*Tomislav_2014-05-30",
				"Checking Novar photo");

		assertTrue("Archived photo exists", !newPhoto.exists());

	}

}
