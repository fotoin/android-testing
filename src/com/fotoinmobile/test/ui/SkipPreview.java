package com.fotoinmobile.test.ui;

import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiSelector;
import com.fotoinmobile.test.ui.util.AppStart;
import com.fotoinmobile.test.ui.util.ClickByAndWait;
import com.fotoinmobile.test.ui.util.FotoINBaseTest;

public class SkipPreview extends FotoINBaseTest {

	public void testPreviewOFF() throws Exception {

		/*
		 * C8609 1.1.3. Photo Capture - Preview is OFF - two photos
		 * C8610 1.1.4. Photo Capture - Preview is OFF - no photos
		 */

		unlockScreen();
		AppStart.slideToUnlock();
		clearData();
		AppStart.startFotoIN(getUiDevice());
		AppStart.skipGuide();
		AppStart.autoSyncOnOff(0);
		gotoGalleryScreen();

		UiObject listView = new UiObject(new UiSelector().description("gallery_photo_list"));
		int numberOfPhotos = listView.getChildCount();

		ClickByAndWait.description("Snap a Photo", "Opening Camera");

		Thread.sleep(2000);

		final UiObject cameraApp = new UiObject(
				new UiSelector().resourceId("com.fotoinmobile.construction:id/camera_view"));
		assertTrue("Not in Camera", cameraApp.exists());

		Thread.sleep(3000);

		final UiObject previewButton = new UiObject(
				new UiSelector().resourceId("com.fotoinmobile.construction:id/camera_preview_button"));
		previewButton.click();

		final UiObject closeButton = new UiObject(
				new UiSelector().resourceId("com.fotoinmobile.construction:id/camera_cancel_button"));
		closeButton.clickAndWaitForNewWindow();

		listView = new UiObject(new UiSelector().description("gallery_photo_list"));

		System.out.println("Checking for photo in gallery");

		if (listView.getChildCount() != numberOfPhotos)
			fail("New photo in gallery");

		ClickByAndWait.description("Snap a Photo", "Opening Camera");

		Thread.sleep(2000);
		assertTrue("Not in Camera", cameraApp.exists());
		Thread.sleep(3000);

		UiObject snapPhoto = new UiObject(
				new UiSelector().resourceId("com.fotoinmobile.construction:id/camera_snap_button"));
		snapPhoto.click();

		while (!snapPhoto.exists())
			Thread.sleep(1000);

		snapPhoto.click();

		while (!snapPhoto.exists())
			Thread.sleep(1000);

		closeButton.clickAndWaitForNewWindow();

		listView = new UiObject(new UiSelector().description("gallery_photo_list"));

		System.out.println("Checking for photos in gallery");

		if (listView.getChildCount() != 2)
			fail("Not all photos in gallery");

	}

	public void testPreviewON() throws Exception {

		/*
		 * 
		 * C8611 1.1.6. Photo Capture - Preview is ON - no photos
		 * C8612 1.1.7. Photo Capture - Preview is ON - two photos
		 * C8613 1.1.8. Photo Capture - Preview is ON - retake feature
		 */

		unlockScreen();
		AppStart.slideToUnlock();
		clearData();
		AppStart.startFotoIN(getUiDevice());
		AppStart.skipGuide();
		AppStart.autoSyncOnOff(0);
		gotoGalleryScreen();

		UiObject listView = new UiObject(new UiSelector().description("gallery_photo_list"));
		int numberOfPhotos = listView.getChildCount();

		ClickByAndWait.description("Snap a Photo", "Opening Camera");

		Thread.sleep(2000);

		final UiObject cameraApp = new UiObject(
				new UiSelector().resourceId("com.fotoinmobile.construction:id/camera_view"));
		assertTrue("Not in Camera", cameraApp.exists());

		Thread.sleep(3000);

		final UiObject closeButton = new UiObject(
				new UiSelector().resourceId("com.fotoinmobile.construction:id/camera_cancel_button"));
		closeButton.clickAndWaitForNewWindow();

		listView = new UiObject(new UiSelector().description("gallery_photo_list"));

		System.out.println("Checking for photo in gallery");

		if (listView.getChildCount() != numberOfPhotos)
			fail("New photo in gallery");

		ClickByAndWait.description("Snap a Photo", "Opening Camera");

		Thread.sleep(2000);
		assertTrue("Not in Camera", cameraApp.exists());
		Thread.sleep(3000);

		UiObject snapPhoto = new UiObject(
				new UiSelector().resourceId("com.fotoinmobile.construction:id/camera_snap_button"));
		snapPhoto.clickAndWaitForNewWindow();

		UiObject retakeButton = new UiObject(
				new UiSelector().resourceId("com.fotoinmobile.construction:id/preview_retake_button"));
		retakeButton.clickAndWaitForNewWindow();

		closeButton.clickAndWaitForNewWindow();

		listView = new UiObject(new UiSelector().description("gallery_photo_list"));

		System.out.println("Checking for photo in gallery");

		if (listView.getChildCount() != numberOfPhotos)
			fail("New photo in gallery");

		ClickByAndWait.description("Snap a Photo", "Opening Camera");

		Thread.sleep(2000);

		assertTrue("Not in Camera", cameraApp.exists());

		Thread.sleep(3000);

	}

}
