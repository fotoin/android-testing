package com.fotoinmobile.test.ui.util;

import com.android.uiautomator.core.UiDevice;
import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiObjectNotFoundException;
import com.android.uiautomator.core.UiScrollable;
import com.android.uiautomator.core.UiSelector;

public class AppStart {

	public static void autoSyncOnOff(final int onOff) throws Exception {

		/*
		 * Turns autosync on if gets 1, turns it off it it gets 0
		 */

		final UiObject autoSyncBox = new UiObject(new UiSelector().className("android.widget.CheckBox").instance(1));

		if (onOff == 1)
			if (!autoSyncBox.isChecked()) {
				System.out.println("Turning Autosync On");
				autoSyncBox.click();
			}

		if (onOff == 0)
			if (autoSyncBox.isChecked()) {
				System.out.println("Turning Autosync Off");
				autoSyncBox.click();
			}

	}

	public static void changeEnvironmentTo(final String env) throws UiObjectNotFoundException {

		final UiObject enviromentValue = new UiObject(new UiSelector().text("Environment").fromParent(
				new UiSelector().index(1)));

		while (!enviromentValue.getText().equals(env))
			ClickByAndWait.text("Environment", "Changing environment");
	}

	public static boolean isAppStarted() {
		final UiObject app = new UiObject(new UiSelector().packageName("com.fotoinmobile.construction").className(
				"android.widget.FrameLayout"));
		return app.exists();
	}

	public static void startFotoIN(final UiDevice device) throws Exception {

		goToHomeScreen(device);
		ClickByAndWait.description("Apps", "Opening All apps");

		System.out.println("Starting FotoIN");
		final UiScrollable allApps = new UiScrollable(new UiSelector().scrollable(true));
		allApps.setAsHorizontalList();
		allApps.scrollTextIntoView("FotoIN");
		final UiObject fotoinApp = new UiObject(new UiSelector().text("FotoIN"));
		fotoinApp.clickAndWaitForNewWindow();
	}

	public static void skipGuide() throws Exception {
		ClickByAndWait.text("Get Started", "Clicking Get Started");

		ClickByAndWait.text("Next", "Clicking Next");

		ClickByAndWait.text("Log in", "Clicking Log in");

		ClickByAndWait.text("Cancel", "Clicking Cancel");

	}

	public static void slideToUnlock() throws Exception {

		final UiObject unlocker = new UiObject(
new UiSelector().resourceId("com.android.systemui:id/lock_icon"));

		if (unlocker.exists())
			unlocker.dragTo(1300, 200, 100);

	}

	public static void goToHomeScreen(final UiDevice device) {

		System.out.println("Checking if on home screen");
		final UiObject HomeScreen = new UiObject(new UiSelector().description("Apps"));
		if (!HomeScreen.exists()) {
			device.pressHome();
			device.waitForIdle();
		}

	}

}
