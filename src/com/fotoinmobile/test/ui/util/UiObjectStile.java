package com.fotoinmobile.test.ui.util;

import java.util.ArrayList;
import java.util.List;

import android.view.accessibility.AccessibilityNodeInfo;

import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiObjectNotFoundException;
import com.android.uiautomator.core.UiSelector;

public class UiObjectStile extends UiObject {

	public UiObjectStile(final UiSelector selector) {
		super(selector);

	}

	@Override
	public String getText() throws UiObjectNotFoundException {
		final AccessibilityNodeInfo node = findAccessibilityNodeInfo(WAIT_FOR_SELECTOR_TIMEOUT);
		if (node == null)
			throw new UiObjectNotFoundException(getSelector().toString());
		final CharSequence text = node.getText();

		final List<Integer> spaces = new ArrayList<Integer>();
		for (int i = 0; i < text.length(); i++)
			if (160 == text.charAt(i))
				spaces.add(i);

		String retVal = text.toString().trim();

		for (final Integer i : spaces)
			retVal = retVal.substring(0, i) + " "
					+ retVal.substring(i + 1, retVal.length());

		return retVal;
	}
}