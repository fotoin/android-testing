package com.fotoinmobile.test.ui.util;

import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.android.uiautomator.core.UiDevice;
import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiObjectNotFoundException;
import com.android.uiautomator.core.UiSelector;

public class HelpingHand {

	public static String isoDate() {

		final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		final String datum = df.format(new Date());
		return datum;
	}

	public static String randomString() {

		final String randomString = Long.toHexString(Double.doubleToLongBits(Math.random()));
		return randomString;
	}

	public static int randomNumber(final int min, int max) {

		max = max - 1;

		final int randomInt = (int) Math.round(Math.random() * (max - min + 1) + min);

		return randomInt;
	}

	@SuppressWarnings("deprecation")
	public static void snapPhoto() throws Exception {
		ClickByAndWait.description("Snap a Photo", "Clicking Snap a Photo button");

		Thread.sleep(2000);

		final UiObject cameraApp = new UiObject(
				new UiSelector().resourceId("com.fotoinmobile.construction:id/camera_view"));
		assertTrue("Not in Camera", cameraApp.exists());

		Thread.sleep(3000); // This sucks

		final UiObject takePhoto = new UiObject(
				new UiSelector().resourceId("com.fotoinmobile.construction:id/camera_snap_button"));
		takePhoto.clickAndWaitForNewWindow();

		final UiObject done = new UiObject(
				new UiSelector().resourceId("com.fotoinmobile.construction:id/preview_edit_button"));
		done.clickAndWaitForNewWindow();
	}

	@SuppressWarnings("deprecation")
	public static void waitForPhotoDownload() throws Exception {
		final UiObject photoDownloadDialog = new UiObject(new UiSelector().text("Photo download"));

		for (int i = 0; photoDownloadDialog.exists(); i++) {
			Thread.sleep(1000);
			if (i == 100) {
				System.out.println("Photo download timeouted");
				fail();
			}
		}

		final UiObject galleryList = new UiObject(new UiSelector().description("gallery_photo_list"));
		if (galleryList.exists()) {
			System.out.println("Photo download failed");
			fail();
		}
	}

	@SuppressWarnings("deprecation")
	public static void getPhotoFromCamera(final UiObject richPhotoButton, final UiDevice device) throws Exception {

		for (int i = 0; !FotoINBaseTest.isInCameraScreen(); i++) {
			if (i >= 20)
				fail("timeout");

			richPhotoButton.click();

			Thread.sleep(200);

			final int dimenzijaX = device.getDisplayWidth();
			final int dimenzijaY = device.getDisplayHeight();

			device.click(dimenzijaX / 2 - 50, dimenzijaY / 2);

			Thread.sleep(3500); // Sucks again

		}
	}

	@SuppressWarnings("deprecation")
	public static void getPhotoFromGallery(final UiObject richPhotoButton, final UiDevice device)
			throws InterruptedException, UiObjectNotFoundException {

		for (int i = 0; !FotoINBaseTest.isInGalleryScreen(); i++) {

			if (i >= 20)
				fail("timeout");

			richPhotoButton.click();

			Thread.sleep(300);

			final int dimenzijaX = device.getDisplayWidth();
			final int dimenzijaY = device.getDisplayHeight();

			device.click(dimenzijaX / 2 + 50, dimenzijaY / 2);
			Thread.sleep(700);
		}

	}

}
