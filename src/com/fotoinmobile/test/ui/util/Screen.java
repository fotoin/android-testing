package com.fotoinmobile.test.ui.util;

import com.android.uiautomator.core.UiDevice;
import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiObjectNotFoundException;
import com.android.uiautomator.core.UiScrollable;
import com.android.uiautomator.core.UiSelector;

public class Screen extends FotoINBaseTest {

	public static void openDrawer(final UiDevice device) throws UiObjectNotFoundException, InterruptedException {

		// UiObject drawerReport = new UiObject(
		// new UiSelector().resourceId("com.fotoinmobile.construction:id/chapter_title_drawer_fragment"));
		// UiObject drawerPhotoEdit = new UiObject(new UiSelector().description("photo_drawer_edit"));
		// UiObject drawerPhotoView = new UiObject(new UiSelector().description("photo_drawer_details"));
		//
		// int i = 0;
		// do {
		//
		// if (drawerReport.exists()) {
		// System.out.println("Drawer is open");
		// Thread.sleep(350);
		// break;
		//
		// }
		//
		// if (drawerPhotoEdit.exists()) {
		// System.out.println("Drawer is open");
		// Thread.sleep(350);
		// break;
		// }
		//
		// if (drawerPhotoView.exists()) {
		// System.out.println("Drawer is open");
		// Thread.sleep(350);
		// break;
		// }
		//
		// Thread.sleep(350);
		//
		// device.drag(0, 800, 800, 800, 100);

		ClickByAndWait.description("Open drawer", "Opening drawer");

		// drawerReport = new UiObject(
		// new UiSelector().resourceId("com.fotoinmobile.construction:id/chapter_title_drawer_fragment"));
		// drawerPhotoEdit = new UiObject(new UiSelector().description("photo_drawer_edit"));
		// drawerPhotoView = new UiObject(new UiSelector().description("photo_drawer_details"));
		//
		// Thread.sleep(350);
		// i++;
		// }
		// while (i < 60);
	}

	public static void closeGalleryDrawer() throws Exception {

		final UiObject photoDrawerDetails = new UiObject(new UiSelector().description("photo_drawer_details"));
		photoDrawerDetails.swipeLeft(50);
	}

	public static void closeReportDrawer() throws UiObjectNotFoundException {
		final UiObject reportDrawer = new UiObject(
				new UiSelector().resourceId("com.fotoinmobile.construction:id/chapter_title_drawer_fragment"));
		reportDrawer.swipeLeft(50);
	}

	public static void showEnviromentButton() throws UiObjectNotFoundException {

		System.out.println("Trying to open Enviroment Button");

		final UiObject settingsText = new UiObject(new UiSelector().text("Settings"));

		if (!settingsText.exists()) {

			FotoINBaseTest.gotoGalleryScreen();
			FotoINBaseTest.gotoSettingsScreen();

		}

		final UiScrollable listView = new UiScrollable(new UiSelector().className("android.widget.ListView"));
		listView.scrollForward();

		final UiObject version = new UiObject(new UiSelector().text("Version"));
		final UiObject deviceID = new UiObject(new UiSelector().text("Device ID"));
		final UiObject copyright = new UiObject(new UiSelector().text("Copyright"));

		for (int i = 0; i < 4; i++)
			version.click();

		for (int i = 0; i < 2; i++)
			deviceID.click();

		for (int i = 0; i < 10; i++)
			copyright.click();

		listView.scrollForward();

	}

}
