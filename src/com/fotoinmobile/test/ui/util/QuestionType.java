package com.fotoinmobile.test.ui.util;

public enum QuestionType {

	PHOTOS(0),

	TEXT(1),

	FLIP_SWITCH(2),

	LIST_SINGLE_CHOICE(3),

	LIST_MULTI_CHOICE(4),

	DATE(5),

	NUMBER(6),

	PROJECT_INFO(7),

	TEXT_RICH(8),

	FLIP_SWITCH_RICH(9),

	LIST_SINGLE_CHOICE_RICH(10),

	LIST_MULTI_CHOICE_RICH(11),

	PROJECT_INFO_PROJECT(12),

	PROJECT_INFO_ADDRESS(13),

	PROJECT_INFO_DEVICE(14),

	PROJECT_INFO_DATE(15),

	DOUBLE_LIST_SINGLE_CHOICE(16),

	DOUBLE_LIST_SINGLE_CHOICE_RICH(17),

	LIST_SINGLE_CHOICE_CUSTOM_LABEL(18),

	LIST_SINGLE_CHOICE_RICH_CUSTOM_LABEL(19),

	LIST_MULTI_CHOICE_CUSTOM_LABEL(20),

	LIST_MULTI_CHOICE_RICH_CUSTOM_LABEL(21);


	private int value;

	private QuestionType(final int p_value) {

		value = p_value;

	}

	public static QuestionType getType(final String p_value) {

		QuestionType result = null;

		for (final QuestionType questionType : values())
			if (questionType.name().equals(p_value))
				result = questionType;

		if (result == null)
			throw new IllegalArgumentException("No QuestionType for value: " + p_value);

		return result;

	}

	public int getValue() {

		return value;

	}

	public boolean isRich() {

		return this == TEXT_RICH || this == FLIP_SWITCH_RICH || this == LIST_SINGLE_CHOICE_RICH
				|| this == LIST_MULTI_CHOICE_RICH

				|| this == DOUBLE_LIST_SINGLE_CHOICE_RICH;

	}

}