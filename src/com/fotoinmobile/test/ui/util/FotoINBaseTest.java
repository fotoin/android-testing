package com.fotoinmobile.test.ui.util;

import android.os.RemoteException;

import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiObjectNotFoundException;
import com.android.uiautomator.core.UiScrollable;
import com.android.uiautomator.core.UiSelector;
import com.android.uiautomator.testrunner.UiAutomatorTestCase;

public class FotoINBaseTest extends UiAutomatorTestCase {

	public static boolean isInGalleryScreen() {
		final UiObject galleryScreen = new UiObject(new UiSelector().description("gallery_photo_list"));
		return galleryScreen.exists();
	}

	public static boolean isInCameraScreen() {
		final UiObject cameraScreen = new UiObject(new UiSelector().description("Shutter"));
		return cameraScreen.exists();
	}

	public static void gotoGalleryScreen() throws UiObjectNotFoundException {
		if (!isInGalleryScreen()) {
			final UiObject navigateUpButton = new UiObject(new UiSelector().descriptionMatches(".*Navigate.*"));
			navigateUpButton.exists();

			navigateUpButton.clickAndWaitForNewWindow();
		}
	}

	public static void gotoSettingsScreen() throws UiObjectNotFoundException {
		gotoGalleryScreen();

		final UiObject optionsButton = new UiObject(new UiSelector().description("More options"));
		optionsButton.exists();
		optionsButton.clickAndWaitForNewWindow();

		final UiObject settingsButton = new UiObject(new UiSelector().text("Settings"));
		settingsButton.exists();
		settingsButton.clickAndWaitForNewWindow();
	}



	protected void clearData() throws Exception {

		System.out.println("Unlocking screen");
		unlockScreen();

		System.out.println("Cleaning data");
		AppStart.goToHomeScreen(getUiDevice());

		ClickByAndWait.description("Apps", "Opening Apps");

		final UiScrollable allApps = new UiScrollable(new UiSelector().scrollable(true));
		allApps.setAsHorizontalList();
		allApps.scrollTextIntoView("Settings");
		ClickByAndWait.text("Settings", "Opening Settings");

		final UiScrollable settings = new UiScrollable(new UiSelector().scrollable(true));
		settings.scrollTextIntoView("Apps");
		ClickByAndWait.text("Apps", "Opening Apps manager");

		System.out.println("Finding FotoIN");
		settings.scrollTextIntoView("FotoIN");
		ClickByAndWait.text("FotoIN", "Clicking FotoIN");

		System.out.println("Clearing data");
		final UiObject clear = new UiObject(new UiSelector().text("Clear data"));
		if (clear.isEnabled())
		{
			clear.clickAndWaitForNewWindow();

			final UiObject ok_button = new UiObject(new UiSelector().text("OK"));
			ok_button.click();

		}
		getUiDevice().pressHome();
		getUiDevice().waitForIdle();

	}

	protected void unlockScreen() throws RemoteException {
		System.out.println("Checking lock screen");
		if (!getUiDevice().isScreenOn()) {
			System.out.println("Unlocking screen");
			getUiDevice().wakeUp();
		}
		getUiDevice().waitForIdle();
	}


	protected void companyLogin(final String companyName, final String companyPasword) throws Exception {
		ClickByAndWait.text("Log In", "Clicking Log in");

		System.out.println("Entering credentials");
		UiObject compamyNameTextbox = new UiObject(
				new UiSelector().resourceId("com.fotoinmobile.construction:id/company_name"));
		compamyNameTextbox.setText(companyName);

		compamyNameTextbox = new UiObject(new UiSelector().resourceId("com.fotoinmobile.construction:id/company_name"));

		for (int i = 0; !compamyNameTextbox.getText().equals(companyName); i++) {
			if (i >= 60)
				fail("timeout");
			compamyNameTextbox.setText(companyName);
			compamyNameTextbox = new UiObject(
					new UiSelector().resourceId("com.fotoinmobile.construction:id/company_name"));

			Thread.sleep(1000);
		}

		final UiObject companyPassword = new UiObject(
				new UiSelector().resourceId("com.fotoinmobile.construction:id/company_password"));
		companyPassword.setText(companyPasword);
		ClickByAndWait.text("OK", "Clicking OK");
		getUiDevice().waitForIdle();

	}

	protected void sync() throws Exception {
		getUiDevice().pressMenu();
		ClickByAndWait.text("Sync All", "Clicking Sync All");

		final UiObject syncIcon = CheckIfUiObjectExistsBy.description("Synchronization", "Checking Sync icon");

		for (int i = 0; syncIcon.exists(); i++){
			Thread.sleep(3000);
			if (i == 100){
				System.out.println("Sync timeouted");
				fail();
			}
		}

		System.out.println("Sync completed");
	}



	protected void longPress(final UiObject object) throws Exception {

		getUiDevice().swipe(object.getVisibleBounds().centerX(), object.getVisibleBounds().centerY(), object.getVisibleBounds().centerX(), object.getVisibleBounds().centerY(), 100);


	}

	protected void syncTest(final String company, final String password, final String provider) throws Exception
	{

		System.out.println("Starting test for " + provider);

		companyLogin(company, password);

		gotoGalleryScreen();
		sync();
		HelpingHand.snapPhoto();
		Screen.openDrawer(getUiDevice());

		final String project = HelpingHand.randomString();
		final String address = HelpingHand.randomString();
		final String comment = HelpingHand.randomString();
		final String annotation = HelpingHand.randomString();

		final UiObject projectValue = new UiObject(new UiSelector().description("Project_value"));
		projectValue.setText(project);
		getUiDevice().pressBack();

		final UiObject addressValue = new UiObject(new UiSelector().description("Address_value"));
		addressValue.setText(address);
		getUiDevice().pressBack();

		ClickByAndWait.text("General", "Opening Const Tags");
		final UiObject tagSelection = new UiObject(new UiSelector().className("android.widget.CheckedTextView")
				.instance(HelpingHand.randomNumber(0, 10)));
		tagSelection.clickAndWaitForNewWindow();

		ClickByAndWait.text("OK", "Clicking OK");

		final UiObject commentValue = new UiObject(new UiSelector().resourceId("com.fotoinmobile.construction:id/photo_meta_comments"));
		commentValue.setText(comment);
		getUiDevice().pressBack();

		ClickByAndWait.text("Done", "Clicking Done");

		final UiObject constTagValue = new UiObject(new UiSelector().description("Tag value master"));

		final String constTagValueString = constTagValue.getText();

		Screen.closeGalleryDrawer();

		ClickByAndWait.description("Tag a Photo", "Adding tag");

		final UiObject annotationValue = new UiObject(new UiSelector().resourceId("com.fotoinmobile.construction:id/annotation_edit_text"));
		annotationValue.setText(annotation);
		getUiDevice().pressBack();
		ClickByAndWait.text("OK", "Clicking OK");

		gotoGalleryScreen();

		Thread.sleep(31000);
		sync();

		getUiDevice().pressHome();

		clearData();
		AppStart.startFotoIN(getUiDevice());
		AppStart.skipGuide();
		AppStart.autoSyncOnOff(0);
		companyLogin(company, password);
		gotoGalleryScreen();
		sync();

		final UiObject photo = CheckIfUiObjectExistsBy
				.textRegex(project + ".*" + address + ".*", "Checking last photo");
		photo.clickAndWaitForNewWindow();
		HelpingHand.waitForPhotoDownload();

		CheckIfUiObjectExistsBy.text(annotation, "Checking annotation");

		Screen.openDrawer(getUiDevice());

		final UiObject viewProjectValue = new UiObject(new UiSelector().description("Project_value_view_mode"));
		final UiObject viewAddressValue = new UiObject(new UiSelector().description("Address_value_view_mode"));
		final UiObject viewCommentValue = new UiObject(new UiSelector().description("Comment_value_view_mode"));
		final UiObject viewTagValue = new UiObject(new UiSelector().description("Tag value master"));

		assertEquals("Project value failed", project, viewProjectValue.getText());
		assertEquals("Address value failed", address, viewAddressValue.getText());
		assertEquals("Comment value failed", comment, viewCommentValue.getText());
		assertEquals("Tag value failed", constTagValueString, viewTagValue.getText());

		gotoGalleryScreen();

		gotoSettingsScreen();

		ClickByAndWait.text("Log out", "Loging out");

		getUiDevice().waitForIdle();

		System.out.println("Test passed for " + provider);


	}

	protected static void failTest(final String reason) {

		fail(reason);

	}

}
