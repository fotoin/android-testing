package com.fotoinmobile.test.ui.util;

import com.android.uiautomator.core.UiDevice;
import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiObjectNotFoundException;
import com.android.uiautomator.core.UiScrollable;
import com.android.uiautomator.core.UiSelector;

public class CustomReportUtil extends FotoINBaseTest {

	public static void checkFlipSwitchButton(final UiObject object, final String label, final int clicked)
			throws UiObjectNotFoundException {
		if (clicked == 0) {
			assertTrue(
					"Property fail",
					object.isCheckable() && !object.isChecked() && object.isClickable() && object.isEnabled()
					&& object.isFocusable() && !object.isFocused());
			label.equals(object.getText());
		}
		if (clicked == 1)
			assertTrue(
					"Property fail",
					object.isCheckable() && object.isChecked() && object.isClickable() && object.isEnabled()
					&& object.isFocusable() && !object.isFocused());
	}

	public static void checkRichButton(final UiObject object) throws UiObjectNotFoundException {
		assertTrue(
				"Property fail",
				!object.isCheckable() && !object.isChecked() && object.isClickable() && object.isEnabled()
				&& !object.isFocusable() && !object.isFocused());
	}

	public static void checkCustomReportButton(final UiObject object, final String label)
			throws UiObjectNotFoundException {
		assertTrue(
				"Property fail",
				!object.isCheckable() && !object.isChecked() && object.isClickable() && object.isEnabled()
				&& object.isFocusable() && !object.isFocused());
		label.equals(object.getText());
	}

	public static void checkSingleChoiceButtons(final UiObject object, final int clicked) throws UiObjectNotFoundException {
		if (clicked == 0)
			assertTrue("Property fail",
					object.isCheckable() && !object.isChecked() && object.isClickable() && object.isEnabled());
		if (clicked == 1)
			assertTrue("Property fail",
					object.isCheckable() && object.isChecked() && object.isClickable() && object.isEnabled());

	}

	public static void checkMultiChoiceButton(final UiObject object, final int clicked) throws UiObjectNotFoundException {
		if (clicked == 0)
			assertTrue("Property fail",
					object.isCheckable() && !object.isChecked() && object.isClickable() && object.isEnabled());
		if (clicked == 1)
			assertTrue("Property fail",
					object.isCheckable() && object.isChecked() && object.isClickable() && object.isEnabled());
	}

	public static void checkReportData(final String descriptionText, final String valueText, final String checking)
			throws UiObjectNotFoundException {

		final UiScrollable reportScroll = new UiScrollable(
				new UiSelector().description("custom_report_chapter_content"));

		reportScroll.scrollIntoView(new UiSelector().description(descriptionText).text(valueText));

		final UiObject textTextboxValue = new UiObject(new UiSelector().description(descriptionText).text(valueText));

		System.out.println("Checking " + checking);

		assertTrue(checking + " doesn't exist!", textTextboxValue.exists());
	}

	public static void uploadPDF(final UiDevice device) throws UiObjectNotFoundException, InterruptedException {

		device.pressBack();
		Thread.sleep(150);
		ClickByAndWait.text("T3 - Subchapters", "Opening T3 - Subchapters");

		final UiObject moreOptionsButton = new UiObject(new UiSelector().description("More options"));
		moreOptionsButton.clickAndWaitForNewWindow();
		ClickByAndWait.text("Export to PDF", "Opening menu, clicking Export to PDF");

		UiObject pdfStatus = new UiObject(new UiSelector().text("PDF is ready."));

		for (int i = 0; !pdfStatus.exists(); i++) {
			if (i >= 60)
				fail("timeout");

			pdfStatus = new UiObject(new UiSelector().text("PDF is ready."));
			Thread.sleep(1000);
		}

		ClickByAndWait.text("Upload", "Clicking Upload");

		for (int i = 0; !pdfStatus.exists(); i++) {
			if (i >= 60)
				fail("timeout");

			pdfStatus = new UiObject(new UiSelector().text("PDF is ready."));
			Thread.sleep(1000);

		}


		device.pressBack();
	}

	protected static void questionTypeSwitch(final int chapterNo, final int subchapterNo, final int questionNo,
			final UiDevice device, final String questionType) throws UiObjectNotFoundException, InterruptedException,
			Exception {
		switch (QuestionType.getType(questionType)) {
		case TEXT:
			CustomReportEnter.textAnswer(questionNo, chapterNo, subchapterNo, device);
			break;
		case TEXT_RICH:
			CustomReportEnter.textAnswerRich(questionNo, chapterNo, subchapterNo, device);
			break;
		case PHOTOS:
			CustomReportEnter.photo(questionNo, chapterNo, subchapterNo, device);
			break;
		case FLIP_SWITCH:
			CustomReportEnter.flipSwitch(questionNo, chapterNo, subchapterNo, device);
			break;
		case FLIP_SWITCH_RICH:
			CustomReportEnter.flipSwitchRich(questionNo, chapterNo, subchapterNo, device);
			break;
		case LIST_SINGLE_CHOICE:
			CustomReportEnter.singleChoice(questionNo, chapterNo, subchapterNo, device);
			break;
		case LIST_SINGLE_CHOICE_RICH:
			CustomReportEnter.singleChoiceRich(questionNo, chapterNo, subchapterNo, device);
			break;
		case LIST_MULTI_CHOICE:
			CustomReportEnter.multiChoice(questionNo, chapterNo, subchapterNo);
			break;
		case LIST_MULTI_CHOICE_RICH:
			CustomReportEnter.multiChoiceRich(questionNo, chapterNo, subchapterNo, device);
			break;
		case DOUBLE_LIST_SINGLE_CHOICE_RICH:
			CustomReportEnter.doubleMulti(questionNo, chapterNo, subchapterNo, device);
			break;
		case LIST_SINGLE_CHOICE_CUSTOM_LABEL:
			CustomReportEnter.singleChoice(questionNo, chapterNo, subchapterNo, device);
			break;
		case LIST_SINGLE_CHOICE_RICH_CUSTOM_LABEL:
			CustomReportEnter.singleChoiceRich(questionNo, chapterNo, subchapterNo, device);
			break;
		case LIST_MULTI_CHOICE_CUSTOM_LABEL:
			CustomReportEnter.multiChoice(questionNo, chapterNo, subchapterNo);
			break;
		case LIST_MULTI_CHOICE_RICH_CUSTOM_LABEL:
			CustomReportEnter.multiChoiceRich(questionNo, chapterNo, subchapterNo, device);
			break;
		default:
			failTest("Wrong question type");
			break;

		}
	}

	public static boolean compareAnswers(String objectDescription, String expectedAnswer)
			throws UiObjectNotFoundException {
		UiObject object = new UiObject(new UiSelector().description(objectDescription));
		if (!expectedAnswer.equals(object.getText()))
			return false;
		return true;

	}

}
