package com.fotoinmobile.test.ui.util;

import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiObjectNotFoundException;
import com.android.uiautomator.core.UiSelector;

public class ClickByAndWait {

	public static void text(final String objectText, final String screenMsg) throws UiObjectNotFoundException {
		System.out.println(screenMsg);
		final UiObject clickObj = new UiObject(new UiSelector().text(objectText));
		clickObj.clickAndWaitForNewWindow();

	}

	public static void description(final String objectText, final String screenMsg) throws UiObjectNotFoundException {
		System.out.println(screenMsg);
		final UiObject clickObj = new UiObject(new UiSelector().description(objectText));
		clickObj.clickAndWaitForNewWindow();

	}

	public static void resourceId(final String objectText, final String screenMsg) throws UiObjectNotFoundException {
		System.out.println(screenMsg);
		final UiObject clickObj = new UiObject(new UiSelector().resourceId(objectText));
		clickObj.clickAndWaitForNewWindow();
	}

	public static void textRegex(final String objectText, final String screenMsg) throws UiObjectNotFoundException {
		System.out.println(screenMsg);
		final UiObject clickObj = new UiObject(new UiSelector().textMatches(objectText));
		clickObj.clickAndWaitForNewWindow();

	}

}
