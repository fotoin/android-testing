package com.fotoinmobile.test.ui.util;

import com.android.uiautomator.core.UiDevice;
import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiObjectNotFoundException;
import com.android.uiautomator.core.UiScrollable;
import com.android.uiautomator.core.UiSelector;

public class CustomReportEnter extends FotoINBaseTest {

	public static void richButtons(final int questionNo, final int chapterNo, final int subchapterNo,
			final UiDevice device)
					throws Exception {

		final String cameraButtonContendDesc = "c" + chapterNo + "-s" + subchapterNo + "-q" + questionNo + "-r1";
		final String commentButtonContendDesc = "c" + chapterNo + "-s" + subchapterNo + "-q" + questionNo + "-r2";
		final String commentBoxContendDesc = "c" + chapterNo + "-s" + subchapterNo + "-q" + questionNo + "-com1";

		final UiObject cameraButton = new UiObject(new UiSelector().description(cameraButtonContendDesc));
		HelpingHand.getPhotoFromCamera(cameraButton, device);
		ClickByAndWait.description("Shutter", "Clicking Shutter");
		ClickByAndWait.description("Done", "Clikcing Done");
		ClickByAndWait.description("Select Photo", "Selecting Photo");

		final UiObject commentButton = new UiObject(new UiSelector().description(commentButtonContendDesc));
		commentButton.click();

		final UiObject commentBox = new UiObject(new UiSelector().description(commentBoxContendDesc));

		if (!commentBox.exists())
			device.swipe(1000, 1100, 1000, 800, 10);
		commentBox.click();
		commentBox.setText(commentBoxContendDesc);
		device.pressBack();

		Thread.sleep(5000);

	}

	public static void textAnswer(int questionNo, final int chapterNo, final int subchapterNo, final UiDevice device)
			throws UiObjectNotFoundException, InterruptedException {

		final String contentDesc = "c" + chapterNo + "-s" + subchapterNo + "-q" + questionNo + "-t1";

		final UiScrollable reportScroll = new UiScrollable(
				new UiSelector().description("custom_report_chapter_content"));

		reportScroll.scrollIntoView(new UiSelector().description(contentDesc));

		final UiObject textBox = new UiObject(new UiSelector().description(contentDesc));
		textBox.click();
		textBox.setText(contentDesc);
		device.pressBack();

		questionNo++;

	}

	public static void textAnswerRich(final int questionNo, final int chapterNo, final int subchapterNo,
			final UiDevice device)
					throws Exception {

		textAnswer(questionNo, chapterNo, subchapterNo, device);

		richButtons(questionNo, chapterNo, subchapterNo, device);

	}

	public static void photo(final int questionNo, final int chapterNo, final int subchapterNo, final UiDevice device)
			throws Exception {

		final String contentDesc = "c" + chapterNo + "-s" + subchapterNo + "-q" + questionNo + "-b1";

		final UiScrollable reportScroll = new UiScrollable(
				new UiSelector().description("custom_report_chapter_content"));

		reportScroll.scrollIntoView(new UiSelector().description(contentDesc));

		final UiObject photoButton = new UiObject(new UiSelector().description(contentDesc));

		int i = 0;

		while (!FotoINBaseTest.isInCameraScreen()) {
			if (i >= 20)
				fail("timeout");

			photoButton.click();

			Thread.sleep(200);

			final int dimenzijaX = device.getDisplayWidth();
			final int dimenzijaY = device.getDisplayHeight();

			device.click(dimenzijaX / 2 - 50, dimenzijaY / 2);

			final UiObject windowTitle = new UiObject(new UiSelector().text("LIST_MULTI_CHOICE"));

			if (windowTitle.exists())
				device.pressBack();

			if (FotoINBaseTest.isInCameraScreen())
				Thread.sleep(3500); // Sucks again

			i++;

		}

		while (!reportScroll.exists()) {

			final UiObject shutterButton = new UiObject(new UiSelector().description("Shutter"));
			final UiObject doneButton = new UiObject(new UiSelector().description("Done"));
			final UiObject selectPhotoButton = new UiObject(new UiSelector().description("Select Photo"));

			if (shutterButton.exists())
				shutterButton.clickAndWaitForNewWindow();
			System.out.println("Clicking Shutter");

			if (doneButton.exists())
				doneButton.clickAndWaitForNewWindow();
			System.out.println("Clikcing Done");

			if (selectPhotoButton.exists())
				selectPhotoButton.clickAndWaitForNewWindow();
			System.out.println("Selecting Photo");
		}
	}

	public static void flipSwitch(final int questionNo, final int chapterNo, final int subchapterNo,
			final UiDevice device)
					throws UiObjectNotFoundException, InterruptedException {

		final String contentDesc = "c" + chapterNo + "-s" + subchapterNo + "-q" + questionNo + "-b1";

		final UiScrollable reportScroll = new UiScrollable(
				new UiSelector().description("custom_report_chapter_content"));

		Screen.openDrawer(device);
		Thread.sleep(350);
		Screen.closeReportDrawer();
		Thread.sleep(350);

		final UiObject flipSwitchYes = new UiObject(new UiSelector().description(contentDesc));

		if (!flipSwitchYes.exists())
			reportScroll.scrollIntoView(new UiSelector().description(contentDesc));

		flipSwitchYes.click();

	}

	public static void flipSwitchRich(final int questionNo, final int chapterNo, final int subchapterNo,
			final UiDevice device)
					throws Exception {
		final String contentDesc = "c" + chapterNo + "-s" + subchapterNo + "-q" + questionNo + "-b2";

		final UiScrollable reportScroll = new UiScrollable(
				new UiSelector().description("custom_report_chapter_content"));

		reportScroll.scrollIntoView(new UiSelector().description(contentDesc));

		final UiObject flipSwitchNo = new UiObject(new UiSelector().description(contentDesc));
		flipSwitchNo.click();

		richButtons(questionNo, chapterNo, subchapterNo, device);

	}

	public static void singleChoice(final int questionNo, final int chapterNo, final int subchapterNo,
			final UiDevice device) throws UiObjectNotFoundException, InterruptedException {

		final String contentDesc = "c" + chapterNo + "-s" + subchapterNo + "-q" + questionNo + "-b1";

		final UiScrollable reportScroll = new UiScrollable(
				new UiSelector().description("custom_report_chapter_content"));

		reportScroll.scrollIntoView(new UiSelector().description(contentDesc));

		Screen.openDrawer(device);
		Thread.sleep(350);
		Screen.closeReportDrawer();
		Thread.sleep(350);

		final UiObject singleChoceButton = new UiObject(new UiSelector().description(contentDesc));
		singleChoceButton.click();

		ClickByAndWait.text("One", "Clickig One");
		ClickByAndWait.text("OK", "Clicing OK");

	}

	public static void singleChoiceRich(final int questionNo, final int chapterNo, final int subchapterNo,
			final UiDevice device)
					throws Exception {

		singleChoice(questionNo, chapterNo, subchapterNo, device);

		richButtons(questionNo, chapterNo, subchapterNo, device);

	}

	public static void multiChoice(final int questionNo, final int chapterNo, final int subchapterNo)
			throws UiObjectNotFoundException {

		final String contentDesc = "c" + chapterNo + "-s" + subchapterNo + "-q" + questionNo + "-b1";

		final UiScrollable reportScroll = new UiScrollable(
				new UiSelector().description("custom_report_chapter_content"));

		reportScroll.scrollIntoView(new UiSelector().description(contentDesc));

		final UiObject multiChoiceButton = new UiObject(new UiSelector().description(contentDesc));

		if (!multiChoiceButton.exists())
			reportScroll.scrollForward(10);

		multiChoiceButton.click();

		ClickByAndWait.text("Three", "Clickig Three");
		ClickByAndWait.text("OK", "Clicing OK");

	}

	public static void multiChoiceRich(final int questionNo, final int chapterNo, final int subchapterNo,
			final UiDevice device)
					throws Exception {

		multiChoice(questionNo, chapterNo, subchapterNo);

		richButtons(questionNo, chapterNo, subchapterNo, device);

	}

	public static void doubleMulti(final int questionNo, final int chapterNo, final int subchapterNo,
			final UiDevice device)
					throws Exception {

		final String contentDescButton1 = "c" + chapterNo + "-s" + subchapterNo + "-q" + questionNo + "-b1";
		final String contentDescButton2 = "c" + chapterNo + "-s" + subchapterNo + "-q" + questionNo + "-b2";

		final UiScrollable reportScroll = new UiScrollable(
				new UiSelector().description("custom_report_chapter_content"));

		reportScroll.scrollIntoView(new UiSelector().description(contentDescButton1));

		final UiObject multiChoiceButton1 = new UiObject(new UiSelector().description(contentDescButton1));
		multiChoiceButton1.click();

		ClickByAndWait.text("Yes", "Clickig One");
		ClickByAndWait.text("OK", "Clicing OK");

		final UiObject multiChoiceButton2 = new UiObject(new UiSelector().description(contentDescButton2));
		multiChoiceButton2.click();

		ClickByAndWait.text("Octarine", "Clickig Octarine");
		ClickByAndWait.text("OK", "Clicing OK");

		richButtons(questionNo, chapterNo, subchapterNo, device);

	}

	public static void autoFillQuestions(final int chapterNo, final int subchapterNo, int questionNo,
			final int maxQuestion, final UiScrollable reportScroll, final UiDevice device)
					throws UiObjectNotFoundException, InterruptedException, Exception {

		String contentDesc;
		UiObject question;
		while (questionNo < maxQuestion) {

			contentDesc = "c" + chapterNo + "-s" + subchapterNo + "-q" + questionNo + "-l1";

			System.out.println("Looking for question " + contentDesc);

			reportScroll.scrollIntoView(new UiSelector().description(contentDesc));

			question = new UiObject(new UiSelector().description(contentDesc));
			final String questionType = question.getText();

			System.out.println("Question " + contentDesc + " is " + questionType);

			CustomReportUtil.questionTypeSwitch(chapterNo, subchapterNo, questionNo, device, questionType);

			questionNo++;
		}
	}

	public static void enterSelectedQuestions(final int chapterNo, final int subchapterNo, final int questionNo,
			final UiDevice device) throws InterruptedException, Exception {

		final UiScrollable reportScroll = new UiScrollable(
				new UiSelector().description("custom_report_chapter_content"));

		final String contentDesc = "c" + chapterNo + "-s" + subchapterNo + "-q" + questionNo + "-l1";

		System.out.println("Looking for question " + contentDesc);

		reportScroll.scrollIntoView(new UiSelector().description(contentDesc));

		Screen.openDrawer(device);
		Thread.sleep(300);
		Screen.closeReportDrawer();
		Thread.sleep(300);

		final UiObject question = new UiObject(new UiSelector().description(contentDesc));

		final String questionType = question.getText();

		System.out.println("Question " + contentDesc + " is " + questionType);

		CustomReportUtil.questionTypeSwitch(chapterNo, subchapterNo, questionNo, device, questionType);

	}

}
