package com.fotoinmobile.test.ui.util;

import java.util.HashMap;
import java.util.Map;

import com.android.uiautomator.core.UiDevice;
import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiObjectNotFoundException;
import com.android.uiautomator.core.UiScrollable;
import com.android.uiautomator.core.UiSelector;

public class CustomReportAnswers {

	public static String singleChoiceTest(final String buttonContentDesc) throws UiObjectNotFoundException {

		UiObject listSingleChoiceButton;
		final UiObject listSingleChoiceOne = CheckIfUiObjectExistsBy.text("One", "Checking choice One");
		CustomReportUtil.checkSingleChoiceButtons(listSingleChoiceOne, 0);

		final UiObject listSingleChoiceTwo = CheckIfUiObjectExistsBy.text("Two", "Checking choice Two");
		CustomReportUtil.checkSingleChoiceButtons(listSingleChoiceTwo, 0);

		final UiObject listSingleChoiceThree = CheckIfUiObjectExistsBy.text("Three", "Checking choice Three");
		CustomReportUtil.checkSingleChoiceButtons(listSingleChoiceThree, 0);

		final UiObject listSingleChoiceFour = CheckIfUiObjectExistsBy.text("Four", "Checking choice Four");
		CustomReportUtil.checkSingleChoiceButtons(listSingleChoiceFour, 0);

		final UiObject listSingleChoiceFive = CheckIfUiObjectExistsBy.text("Five", "Checking choice Five");
		CustomReportUtil.checkSingleChoiceButtons(listSingleChoiceFive, 0);

		final UiObject singleOK = CheckIfUiObjectExistsBy.text("OK", "Checking OK buttton");
		CustomReportUtil.checkCustomReportButton(singleOK, "OK");

		final UiObject singleCancel = CheckIfUiObjectExistsBy.text("Cancel", "Checking Cancel buttton");
		CustomReportUtil.checkCustomReportButton(singleCancel, "Cancel");

		System.out.println("Choosing a random answer");
		UiObject singleChoiceSelection = new UiObject(new UiSelector().resourceId("android:id/text1").instance(
				HelpingHand.randomNumber(0, 4)));
		singleChoiceSelection.click();
		String singleChoiceSelectionText = singleChoiceSelection.getText();

		System.out.println("Checking answer");
		CustomReportUtil.checkSingleChoiceButtons(singleChoiceSelection, 1);

		System.out.println("Clicking OK");
		singleOK.clickAndWaitForNewWindow();

		System.out.println("Checking new answer label");
		listSingleChoiceButton = new UiObject(new UiSelector().description(buttonContentDesc));

		CustomReportUtil.checkCustomReportButton(listSingleChoiceButton, singleChoiceSelectionText);

		System.out.println("Choosing another random question");
		listSingleChoiceButton.click();

		singleChoiceSelection = new UiObject(new UiSelector().resourceId("android:id/text1").instance(
				HelpingHand.randomNumber(0, 4)));
		singleChoiceSelectionText = singleChoiceSelection.getText();

		singleChoiceSelection.click();
		singleOK.clickAndWaitForNewWindow();

		listSingleChoiceButton = new UiObject(new UiSelector().description(buttonContentDesc));

		System.out.println("Checking new answer label");
		CustomReportUtil.checkCustomReportButton(listSingleChoiceButton, singleChoiceSelectionText);

		return singleChoiceSelectionText;
	}

	public static String multiChoiceTest(final UiObject listMultiButton) throws UiObjectNotFoundException {
		System.out.println("Checking options");

		final UiObject listMultiOne = CheckIfUiObjectExistsBy.text("One", "Checking choice One");
		CustomReportUtil.checkMultiChoiceButton(listMultiOne, 0);

		final UiObject listMultiTwo = CheckIfUiObjectExistsBy.text("Two", "Checking choice Two");
		CustomReportUtil.checkMultiChoiceButton(listMultiTwo, 0);

		final UiObject listMultiThree = CheckIfUiObjectExistsBy.text("Three", "Checking choice Three");
		CustomReportUtil.checkMultiChoiceButton(listMultiThree, 0);

		final UiObject listMultiFour = CheckIfUiObjectExistsBy.text("Four", "Checking choice Four");
		CustomReportUtil.checkMultiChoiceButton(listMultiFour, 0);

		final UiObject listMultiFive = CheckIfUiObjectExistsBy.text("Five", "Checking choice Five");
		CustomReportUtil.checkMultiChoiceButton(listMultiFive, 0);

		// Getting random answers
		String newLabel = multiChoiceRandomAnswers();

		System.out.println("Checking new label");
		CustomReportUtil.checkCustomReportButton(listMultiButton, newLabel);

		System.out.println("Deselecting answers");
		listMultiButton.clickAndWaitForNewWindow();
		ClickByAndWait.text("Deselect", "Clicking Deselect");

		System.out.println("Checking new label");
		newLabel = multiChoiceRandomAnswers();
		CustomReportUtil.checkCustomReportButton(listMultiButton, newLabel);

		return newLabel;
	}

	public static String multiChoiceRandomAnswers() throws UiObjectNotFoundException {
		final String separator = ", ";

		final Map<Integer, String> answers = new HashMap<Integer, String>();

		for (int i = 0; i < 5; i++) {

			final UiObject answer = new UiObject(new UiSelector().resourceId("android:id/text1").instance(i));

			answers.put(i, answer.getText());

		}

		int[] order;
		order = new int[3];

		final int random1 = HelpingHand.randomNumber(0, 4);

		order[0] = random1;

		int random2 = HelpingHand.randomNumber(0, 4);
		while (random1 == random2)
			random2 = HelpingHand.randomNumber(0, 4);

		order[1] = random2;

		int random3 = HelpingHand.randomNumber(0, 4);
		while (random1 == random3 || random2 == random3)
			random3 = HelpingHand.randomNumber(0, 4);

		order[2] = random3;

		int sort = 1;
		int temp;
		while (sort == 1) {
			sort = 0;
			for (int i = 0; i < 2; i++)
				if (order[i] > order[i + 1]) {
					temp = order[i];
					order[i] = order[i + 1];
					order[i + 1] = temp;
					sort = 1;
				}

		}

		final String newLabel = answers.get(order[0]) + separator + answers.get(order[1]) + separator
				+ answers.get(order[2]);

		System.out.println("Choosing and checking answers");
		UiObject answer = new UiObject(new UiSelector().resourceId("android:id/text1").instance(random1));
		answer.click();
		CustomReportUtil.checkMultiChoiceButton(answer, 1);

		answer = new UiObject(new UiSelector().resourceId("android:id/text1").instance(random2));
		answer.click();
		CustomReportUtil.checkMultiChoiceButton(answer, 1);

		answer = new UiObject(new UiSelector().resourceId("android:id/text1").instance(random3));
		answer.click();
		CustomReportUtil.checkMultiChoiceButton(answer, 1);

		ClickByAndWait.text("OK", "Clicking OK");

		return newLabel;
	}

	public static void inputTextAnswer(final String description, final UiDevice device)
			throws UiObjectNotFoundException {
		final UiScrollable reportScroll = new UiScrollable(
				new UiSelector().description("custom_report_chapter_content"));

		reportScroll.scrollIntoView(new UiSelector().description(description));

		final UiObject textTextbox = CheckIfUiObjectExistsBy.description(description, "Checking TEXT textbox");
		enterText(description, textTextbox, "TEXT", device);
	}


	public static void testRichButtons(final String contentDescCommentButton, final String contentDescCommentTextbox,
			final String contentDescPhotoButton, final String questionType, final String source,
			final String projectNameString, final String addressString, final UiDevice device)
					throws UiObjectNotFoundException, Exception {

		final UiObject cameraButton = CheckIfUiObjectExistsBy.description(contentDescPhotoButton, "Checking "
				+ questionType + " camera button");
		CustomReportUtil.checkRichButton(cameraButton);

		Thread.sleep(350);

		if (source == "Camera") {

			HelpingHand.getPhotoFromCamera(cameraButton, device);

			ClickByAndWait.description("Shutter", "Clicking Shutter");
			ClickByAndWait.description("Done", "Clikcing Done");
			ClickByAndWait.description("Select Photo", "Selecting Photo");

		}

		if (source == "Gallery") {
			HelpingHand.getPhotoFromGallery(cameraButton, device);

			final UiObject newPhoto = CheckIfUiObjectExistsBy.textRegex(projectNameString + "_" + addressString + ".*_"
					+ HelpingHand.isoDate(), "Checking photo in gallery");
			newPhoto.clickAndWaitForNewWindow();
			ClickByAndWait.description("Select Photo", "Selecting photo");
		}

		final UiObject commentButton = CheckIfUiObjectExistsBy.description(contentDescCommentButton, "Checking "
				+ questionType + " comment button");
		CustomReportUtil.checkRichButton(commentButton);
		commentButton.click();

		final UiObject commentTextbox = CheckIfUiObjectExistsBy.description(contentDescCommentTextbox, "Checking "
				+ questionType + " comment textbox");
		enterText(contentDescCommentButton, commentTextbox, "COMMENT", device);

	}

	protected static void enterText(final String description, final UiObject textTextbox, final String type,
			final UiDevice device)
					throws UiObjectNotFoundException {
		textTextbox.click();
		System.out.println("Entering " + type + " answer");
		textTextbox.setText(description);
		device.pressBack();
	}
}
