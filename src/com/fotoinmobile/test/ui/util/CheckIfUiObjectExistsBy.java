package com.fotoinmobile.test.ui.util;

import junit.framework.Assert;

import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiSelector;

@SuppressWarnings("deprecation")
public class CheckIfUiObjectExistsBy {

	public static UiObject text(final String objectText, final String screenMsg) {
		System.out.println(screenMsg);
		final UiObject obj = new UiObject(new UiSelector().text(objectText));
		assertExists(obj);
		return obj;
	}

	public static UiObject textRegex(final String objectText, final String screenMsg) {
		System.out.println(screenMsg);
		final UiObject obj = new UiObject(new UiSelector().textMatches(objectText));
		assertExists(obj);
		return obj;
	}

	public static UiObject description(final String objectDescription, final String screenMsg) {
		System.out.println(screenMsg);
		final UiObject obj = new UiObject(new UiSelector().description(objectDescription));
		assertExists(obj);
		return obj;
	}

	public static UiObject descriptionRegex(final String objectDescription, final String screenMsg) {
		System.out.println(screenMsg);
		final UiObject obj = new UiObject(new UiSelector().descriptionMatches(objectDescription));
		assertExists(obj);
		return obj;
	}

	public static UiObject resourceId(final String objectDescription, final String screenMsg) {
		System.out.println(screenMsg);
		final UiObject obj = new UiObject(new UiSelector().resourceId(objectDescription));
		assertExists(obj);
		return obj;
	}

	private static void assertExists(final UiObject object) {
		Assert.assertTrue("Doestn't exists", object.exists());
	}

}
