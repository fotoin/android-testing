package com.fotoinmobile.test.ui;

import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiSelector;
import com.fotoinmobile.test.ui.util.AppStart;
import com.fotoinmobile.test.ui.util.CheckIfUiObjectExistsBy;
import com.fotoinmobile.test.ui.util.ClickByAndWait;
import com.fotoinmobile.test.ui.util.CustomReportUtil;
import com.fotoinmobile.test.ui.util.FotoINBaseTest;
import com.fotoinmobile.test.ui.util.HelpingHand;
import com.fotoinmobile.test.ui.util.Screen;

public class BasicReport extends FotoINBaseTest {

	public void testBasicReport() throws Exception {
		unlockScreen();
		AppStart.slideToUnlock();
		clearData();
		AppStart.startFotoIN(getUiDevice());
		AppStart.skipGuide();
		AppStart.autoSyncOnOff(0);
		companyLogin("uitesting", "123456");
		gotoGalleryScreen();
		sync();

		HelpingHand.snapPhoto();
		Screen.openDrawer(getUiDevice());

		UiObject projectValue = new UiObject(new UiSelector().description("Project_value"));
		String project = projectValue.getText();

		UiObject addressValue = new UiObject(new UiSelector().description("Address_value"));
		String address = addressValue.getText();

		final UiObject deviceName = new UiObject(new UiSelector().description("Device_value"));
		String device = deviceName.getText();

		final UiObject refreshAddressButton = new UiObject(new UiSelector().description("Find adresss_button"));

		while (project.equals("Unknown") || address.equals("Unknown")) {
			refreshAddressButton.click();

			Thread.sleep(200);

			projectValue = new UiObject(new UiSelector().description("Project_value"));
			project = projectValue.getText();

			addressValue = new UiObject(new UiSelector().description("Address_value"));
			address = addressValue.getText();

			System.out.println("Project name is " + project + ", address is " + address);
		}

		ClickByAndWait.description("Photo View, Navigate up", "Returning to gallery");

		ClickByAndWait.description("More options", "Opening options");

		ClickByAndWait.text("Start a Report", "Opening reports");

		ClickByAndWait.text("Basic report", "Starting Basic Report");

		if (CustomReportUtil.compareAnswers("c1-s1-q1-t1", project))
			System.out.println("Project name OK");
		else
			fail("Invalid project name!");

		if (CustomReportUtil.compareAnswers("c1-s1-q1-t2", address))
			System.out.println("Address OK");
		else
			fail("Invalid address!");

		if (CustomReportUtil.compareAnswers("c1-s1-q1-t3", device))
			System.out.println("Device name OK");
		else
			fail("Invalid device name");

		final UiObject conclusionsText = new UiObject(new UiSelector().description("c1-s1-q2-t1"));
		conclusionsText.click();
		String conclusionsAnswer = "Zakljucak zakljucaka";
		conclusionsText.setText(conclusionsAnswer);
		getUiDevice().pressBack();

		final UiObject actionStepsText = new UiObject(new UiSelector().description("c1-s1-q3-t1"));
		actionStepsText.click();
		String actionStepsAnswer = "Akcijski koraci";
		actionStepsText.setText(actionStepsAnswer);
		getUiDevice().pressBack();

		final UiObject photosButton = new UiObject(new UiSelector().description("c1-s1-q4-b1"));
		HelpingHand.getPhotoFromCamera(photosButton, getUiDevice());
		ClickByAndWait.description("Shutter", "Clicking Shutter");
		ClickByAndWait.description("Done", "Clikcing Done");
		ClickByAndWait.description("Select Photo", "Selecting Photo");

		final UiObject photo = new UiObject(
				new UiSelector().resourceId("com.fotoinmobile.construction:id/report_photo_thumb"));

		ClickByAndWait.text("Basic report", "Exiting report");

		ClickByAndWait.text("Basic report", "Reopening report");

		if (CustomReportUtil.compareAnswers("c1-s1-q1-t1", project))
			System.out.println("Project name OK");
		else
			fail("Invalid project name!");

		if (CustomReportUtil.compareAnswers("c1-s1-q1-t2", address))
			System.out.println("Address OK");
		else
			fail("Invalid address!");

		if (CustomReportUtil.compareAnswers("c1-s1-q1-t3", device))
			System.out.println("Device name OK");
		else
			fail("Invalid device name");

		if (CustomReportUtil.compareAnswers("c1-s1-q2-t1", conclusionsAnswer))
			System.out.println("Conclusion answer OK");
		else
			fail("Invalid conclusion answer");

		if (CustomReportUtil.compareAnswers("c1-s1-q3-t1", actionStepsAnswer))
			System.out.println("Action Steps answer OK");
		else
			fail("Invalid Action Steps answer");

		ClickByAndWait.description("More options", "Opening Menu");

		ClickByAndWait.text("Export to PDF", "Exporting to PDF");

		final UiObject uploadButton = CheckIfUiObjectExistsBy.text("Upload", "Checking Upload button");

		uploadButton.click();

		while (!uploadButton.exists())
			Thread.sleep(300);

		getUiDevice().pressBack();

		ClickByAndWait.description("More options", "Opening Menu");

		ClickByAndWait.text("Reset", "Reseting report");

		if (CustomReportUtil.compareAnswers("c1-s1-q1-t1", project))
			System.out.println("Project name OK");
		else {
			UiObject object = new UiObject(new UiSelector().description("c1-s1-q1-t1"));
			System.out.println("Was " + project + " and now is " + object.getText());
			fail("Invalid project name!");
		}

		if (CustomReportUtil.compareAnswers("c1-s1-q1-t2", address))
			System.out.println("Address OK");
		else
			fail("Invalid address!");

		if (CustomReportUtil.compareAnswers("c1-s1-q1-t3", device))
			System.out.println("Device name OK");
		else
			fail("Invalid device name");

		if (!CustomReportUtil.compareAnswers("c1-s1-q2-t1", conclusionsAnswer))
			System.out.println("Conclusion answer OK");
		else
			fail("Invalid conclusion answer");

		if (!CustomReportUtil.compareAnswers("c1-s1-q3-t1", actionStepsAnswer))
			System.out.println("Action Steps answer OK");
		else
			fail("Invalid Action Steps answer");

		assertTrue("Photo exists", !photo.exists());
	}

}
