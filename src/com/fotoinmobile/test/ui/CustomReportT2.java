package com.fotoinmobile.test.ui;

import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiScrollable;
import com.android.uiautomator.core.UiSelector;
import com.fotoinmobile.test.ui.util.AppStart;
import com.fotoinmobile.test.ui.util.ClickByAndWait;
import com.fotoinmobile.test.ui.util.CustomReportEnter;
import com.fotoinmobile.test.ui.util.CustomReportUtil;
import com.fotoinmobile.test.ui.util.FotoINBaseTest;
import com.fotoinmobile.test.ui.util.HelpingHand;
import com.fotoinmobile.test.ui.util.Screen;

public class CustomReportT2 extends FotoINBaseTest {

	/*
	 * 
	 * C4586 2.5. Master Testing Report - T2 - Render the report
	 * C4587 2.5.1. Master Testing Report - T2 - All Questions
	 * C4588 2.5.2. Master Testing Report - T2 - Chapter 1
	 * C4589 2.5.3. Master Testing Report - T2 - Chapter 2
	 * C4590 2.5.4. Master Testing Report - T2 - Chapter 4
	 * C4594 2.5.6. Master Testing Report - T2 - Select Questions
	 */

	public void testCustomReportT2() throws Exception {

		unlockScreen();
		AppStart.slideToUnlock();
		clearData();
		AppStart.startFotoIN(getUiDevice());
		AppStart.skipGuide();
		companyLogin("uitesting", "123456");
		AppStart.autoSyncOnOff(0);
		gotoGalleryScreen();
		sync();

		HelpingHand.snapPhoto();

		Screen.openDrawer(getUiDevice());
		;

		UiObject projectName = new UiObject(new UiSelector().description("Project_value"));

		while (projectName.getText().equals("Unknown")) {

			ClickByAndWait.description("Find GPS_button", "Refreshing GPS");
			Thread.sleep(1500);
			ClickByAndWait.description("Find adresss_button", "Refreshing Address");
		}

		ClickByAndWait.description("Done_button", "Clicking Done");

		projectName = new UiObject(new UiSelector().description("Project_value_view_mode"));

		final String projectNameString = projectName.getText();

		gotoGalleryScreen();

		getUiDevice().pressMenu();
		ClickByAndWait.text("Start a Report", "Opening reports");

		ClickByAndWait.text("T2 - Chapters", "Opening T2 - Chapters");

		int chapterNo = 1;
		int questionNo = 2;
		final int subchapterNo = 1;

		int maxQuestion = 13;

		UiObject projectNameValue = new UiObject(new UiSelector().description("c1-s1-q1-t1"));

		System.out.println("Waiting for report");

		for (int i = 0; !projectNameValue.getText().equals(projectNameString); i++) {
			if (i >= 60)
				fail("timeout");

			projectNameValue = new UiObject(new UiSelector().description("c1-s1-q1-t1"));
			Thread.sleep(1000);
		}

		final UiObject moreOptionsButton = new UiObject(new UiSelector().description("More options"));

		final UiScrollable reportScroll = new UiScrollable(
				new UiSelector().description("custom_report_chapter_content"));

		while (chapterNo < 5) {


			switch (chapterNo) {
			case 1:
				CustomReportEnter.autoFillQuestions(chapterNo, subchapterNo, questionNo, maxQuestion, reportScroll,
						getUiDevice());

				CustomReportUtil.uploadPDF(getUiDevice());
				System.out.println("C4588 2.5.2. Master Testing Report - T2 - Chapter 1 passed"); // C4588 2.5.2. Master Testing Report - T2 - Chapter 1

				break;
			case 2:
				Screen.openDrawer(getUiDevice());
				ClickByAndWait.text("Chapter with Header", "Opening Chapter \"Chapter with Header\"");
				questionNo = 1;
				maxQuestion = 10;
				CustomReportEnter.autoFillQuestions(chapterNo, subchapterNo, questionNo, maxQuestion, reportScroll,
						getUiDevice());
				break;

			case 3:
				Screen.openDrawer(getUiDevice());
				;
				ClickByAndWait.text("Chapter with Footer", "Opening Chapter \"Chapter with Footer\"");
				questionNo = 1;
				maxQuestion = 11;
				CustomReportEnter.autoFillQuestions(chapterNo, subchapterNo, questionNo, maxQuestion, reportScroll,
						getUiDevice());
				break;

			case 4:
				Screen.openDrawer(getUiDevice());
				;
				ClickByAndWait.text("Chapter with no Header or Footer",
						"Opening Chapter \"Chapter with no Header or Footer\"");
				questionNo = 1;
				maxQuestion = 9;
				CustomReportEnter.autoFillQuestions(chapterNo, subchapterNo, questionNo, maxQuestion, reportScroll,
						getUiDevice());

				CustomReportUtil.uploadPDF(getUiDevice());
				System.out.println("C4587 2.5.1. Master Testing Report - T2 - All Questions passed"); // C4587 2.5.1. Master Testing Report - T2 - All Questions
				break;

			default:
				break;
			}

			chapterNo++;

			System.out.println("ChapterNo increased to :" + chapterNo);

		}

		// C4589 2.5.3. Master Testing Report - T2 - Chapter 2

		moreOptionsButton.clickAndWaitForNewWindow();
		ClickByAndWait.text("Reset", "Reseting report");

		chapterNo = 2;
		questionNo = 1;
		maxQuestion = 10;

		Screen.openDrawer(getUiDevice());
		;
		ClickByAndWait.text("Chapter with Header", "Opening Chapter \"Chapter with Header\"");

		CustomReportEnter.autoFillQuestions(chapterNo, subchapterNo, questionNo, maxQuestion, reportScroll,
				getUiDevice());

		CustomReportUtil.uploadPDF(getUiDevice());
		System.out.println("C4589 2.5.3. Master Testing Report - T2 - Chapter 2 passed");

		// C4590 2.5.4. Master Testing Report - T2 - Chapter 4

		moreOptionsButton.clickAndWaitForNewWindow();
		ClickByAndWait.text("Reset", "Reseting report");

		chapterNo = 4;
		questionNo = 1;
		maxQuestion = 9;

		Screen.openDrawer(getUiDevice());
		;
		ClickByAndWait.text("Chapter with no Header or Footer", "Opening Chapter \"Chapter with no Header or Footer\"");

		CustomReportEnter.autoFillQuestions(chapterNo, subchapterNo, questionNo, maxQuestion, reportScroll,
				getUiDevice());

		CustomReportUtil.uploadPDF(getUiDevice());

		moreOptionsButton.clickAndWaitForNewWindow();
		ClickByAndWait.text("Reset", "Reseting report");

		System.out.println("C4590 2.5.4. Master Testing Report - T2 - Chapter 4 passed");

		// C4594 2.5.6. Master Testing Report - T2 - Select Questions

		final int[] chapterOne = {3,5,7};
		final int[] chapterTwo = {2,4,6};
		final int[] chapterThree = {1,3,5};
		final int[] chapterFour = {2,3,5,8};

		chapterNo = 1;

		for (final int element : chapterOne)
			CustomReportEnter.enterSelectedQuestions(chapterNo, subchapterNo, element, getUiDevice());

		chapterNo++;

		Screen.openDrawer(getUiDevice());
		;
		ClickByAndWait.text("Chapter with Header", "Opening Chapter \"Chapter with Header\"");
		for (final int element : chapterTwo)
			CustomReportEnter.enterSelectedQuestions(chapterNo, subchapterNo, element, getUiDevice());

		chapterNo++;

		Screen.openDrawer(getUiDevice());
		;
		ClickByAndWait.text("Chapter with Footer", "Opening Chapter \"Chapter with Footer\"");

		for (final int element : chapterThree)
			CustomReportEnter.enterSelectedQuestions(chapterNo, subchapterNo, element, getUiDevice());

		chapterNo++;

		Screen.openDrawer(getUiDevice());
		;
		ClickByAndWait.text("Chapter with no Header or Footer", "Opening Chapter \"Chapter with no Header or Footer\"");

		for (final int element : chapterFour)
			CustomReportEnter.enterSelectedQuestions(chapterNo, subchapterNo, element, getUiDevice());

		chapterNo++;

		CustomReportUtil.uploadPDF(getUiDevice());


	}

}

