package com.fotoinmobile.test.ui;

import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiScrollable;
import com.android.uiautomator.core.UiSelector;
import com.fotoinmobile.test.ui.util.AppStart;
import com.fotoinmobile.test.ui.util.CheckIfUiObjectExistsBy;
import com.fotoinmobile.test.ui.util.ClickByAndWait;
import com.fotoinmobile.test.ui.util.CustomReportEnter;
import com.fotoinmobile.test.ui.util.CustomReportUtil;
import com.fotoinmobile.test.ui.util.FotoINBaseTest;
import com.fotoinmobile.test.ui.util.HelpingHand;
import com.fotoinmobile.test.ui.util.Screen;

public class CustomReportT3 extends FotoINBaseTest {

	public void testCustomReportT3() throws Exception {

		/*
		 * C4596 2.6.1. Master Testing Report - T3 - All Questions
		 * C4597 2.6.2. Master Testing Report - T3 - Chapter 1 - Subchapter 1
		 */

		unlockScreen();
		AppStart.slideToUnlock();
		clearData();
		AppStart.startFotoIN(getUiDevice());
		AppStart.skipGuide();
		companyLogin("uitesting", "123456");

		AppStart.autoSyncOnOff(0);
		gotoGalleryScreen();
		sync();

		HelpingHand.snapPhoto();

		Screen.openDrawer(getUiDevice());

		UiObject projectName = new UiObject(new UiSelector().description("Project_value"));

		while (projectName.getText().equals("Unknown")) {

			ClickByAndWait.description("Find GPS_button", "Refreshing GPS");
			Thread.sleep(1500);
			ClickByAndWait.description("Find adresss_button", "Refreshing Address");
		}

		ClickByAndWait.description("Done_button", "Clicking Done");

		projectName = new UiObject(new UiSelector().description("Project_value_view_mode"));

		final String projectNameString = projectName.getText();

		gotoGalleryScreen();

		getUiDevice().pressMenu();
		ClickByAndWait.text("Start a Report", "Opening reports");
		ClickByAndWait.text("T3 - Subchapters", "Opening T3 - Subchapters");

		UiObject projectNameValue = new UiObject(new UiSelector().description("c1-s1-q1-t1"));

		int chapterNo = 1;
		int questionNo = 5;
		int subchapterNo = 1;

		final int maxQuestion = 10;

		System.out.println("Waiting for report");

		for (int i = 0; !projectNameValue.getText().equals(projectNameString); i++) {
			if (i >= 60)
				fail("timeout");

			Thread.sleep(300);

			ClickByAndWait.description("Subchapter I, Navigate up", "Returning to Report Menu");

			ClickByAndWait.text("T3 - Subchapters", "Opening T3 - Subchapters");

			projectNameValue = new UiObject(new UiSelector().description("c1-s1-q1-t1"));
			Thread.sleep(700);
		}

		final UiScrollable reportScroll = new UiScrollable(
				new UiSelector().description("custom_report_chapter_content"));

		while (chapterNo < 5) {

			subchapterNo = 1;

			while (subchapterNo < 5) {

				while (questionNo < maxQuestion) {

					CustomReportEnter.enterSelectedQuestions(chapterNo, subchapterNo, questionNo, getUiDevice());

					if (!reportScroll.exists())
						if (CheckIfUiObjectExistsBy.text("Start a Report", "Checking if in Report Menu Screen") != null)
							ClickByAndWait.text("T3 - Subchapters", "Returning to report");

					questionNo++;
				}

				// C4597 2.6.2. Master Testing Report - T3 - Chapter 1 - Subchapter 1
				if (chapterNo == 1 && subchapterNo == 1)
					CustomReportUtil.uploadPDF(getUiDevice());

				if (chapterNo == 4 && subchapterNo == 4)
					CustomReportUtil.uploadPDF(getUiDevice());

				subchapterNo++;

				if (chapterNo == 1)
					questionNo = 5;
				else
					questionNo = 1;

				switch (subchapterNo) {
				case 2:
					Screen.openDrawer(getUiDevice());
					ClickByAndWait.text("Subchapter II", "Opening chapter " + chapterNo + ", subchapter "
							+ subchapterNo);
					break;
				case 3:
					Screen.openDrawer(getUiDevice());
					ClickByAndWait.text("Subchapter III", "Opening chapter " + chapterNo + ", subchapter "
							+ subchapterNo);
					break;
				case 4:
					Screen.openDrawer(getUiDevice());
					ClickByAndWait.text("Subchapter IV", "Opening chapter " + chapterNo + ", subchapter "
							+ subchapterNo);
					break;
				default:
					break;
				}

			}

			chapterNo++;

			switch (chapterNo) {
			case 2:
				questionNo = 1;
				Screen.openDrawer(getUiDevice());
				ClickByAndWait.text("Chapter with Header", "Opening Chapter with Header");
				break;
			case 3:
				Screen.openDrawer(getUiDevice());
				ClickByAndWait.text("Chapter with Footer", "Opening Chapter with Footer");
				break;
			case 4:
				Screen.openDrawer(getUiDevice());
				ClickByAndWait.text("Chapter with no Header or Footer", "Opening Chapter with no Header or Footer");
				break;
			default:
				break;
			}

		}
	}
}
