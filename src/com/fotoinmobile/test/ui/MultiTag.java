package com.fotoinmobile.test.ui;

import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiSelector;
import com.fotoinmobile.test.ui.util.AppStart;
import com.fotoinmobile.test.ui.util.CustomReportAnswers;
import com.fotoinmobile.test.ui.util.FotoINBaseTest;
import com.fotoinmobile.test.ui.util.HelpingHand;
import com.fotoinmobile.test.ui.util.Screen;

public class MultiTag extends FotoINBaseTest {

	public void testMultiTag() throws Exception {

		unlockScreen();
		AppStart.slideToUnlock();
		clearData();
		AppStart.startFotoIN(getUiDevice());
		AppStart.skipGuide();
		AppStart.autoSyncOnOff(0);
		companyLogin("uitesting", "123456");
		gotoGalleryScreen();
		sync();

		HelpingHand.snapPhoto();
		Screen.openDrawer(getUiDevice());

		UiObject multiTag = new UiObject(new UiSelector().description("Tag button Test 1"));
		multiTag.clickAndWaitForNewWindow();

		String newTags = CustomReportAnswers.multiChoiceRandomAnswers();

		assertEquals(newTags, multiTag.getText());
	}

}
