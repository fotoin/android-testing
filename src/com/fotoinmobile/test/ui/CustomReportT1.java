package com.fotoinmobile.test.ui;

import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiScrollable;
import com.android.uiautomator.core.UiSelector;
import com.fotoinmobile.test.ui.util.AppStart;
import com.fotoinmobile.test.ui.util.CheckIfUiObjectExistsBy;
import com.fotoinmobile.test.ui.util.ClickByAndWait;
import com.fotoinmobile.test.ui.util.CustomReportAnswers;
import com.fotoinmobile.test.ui.util.CustomReportUtil;
import com.fotoinmobile.test.ui.util.FotoINBaseTest;
import com.fotoinmobile.test.ui.util.HelpingHand;
import com.fotoinmobile.test.ui.util.Screen;

public class CustomReportT1 extends FotoINBaseTest {

	public void testCustomReportT1() throws Exception {

		/*
		 * C4581 2.4.1. Master Testing Report - T1 - All Questions
		 * C4582 2.4.2. Master Testing Report - T1 - Data Persistence
		 * C4580 2.4. Master Testing Report - T1 - Render the report
		 * C7119 2.4.4. MasterTesting Report - T1 - Reset the report
		 */

		final String defaultTextValue = "Enter your answer here";

		unlockScreen();
		AppStart.slideToUnlock();
		clearData();
		AppStart.startFotoIN(getUiDevice());
		AppStart.skipGuide();
		companyLogin("uitesting", "123456");
		AppStart.autoSyncOnOff(0);
		gotoGalleryScreen();
		sync();

		HelpingHand.snapPhoto();

		Screen.openDrawer(getUiDevice());

		UiObject projectName = new UiObject(
				new UiSelector().description("Project_value"));

		while (projectName.getText().equals("Unknown")) {

			ClickByAndWait.description("Find GPS_button", "Refreshing GPS");
			Thread.sleep(1500);
			ClickByAndWait.description("Find adresss_button",
					"Refreshing Address");
		}

		ClickByAndWait.description("Done_button", "Clicking Done");

		projectName = new UiObject(
				new UiSelector().description("Project_value_view_mode"));

		final UiObject address = new UiObject(
				new UiSelector().description("Address_value_view_mode"));

		final UiObject deviceName = new UiObject(
				new UiSelector().description("Device_value_view_mode"));

		final String deviceNameString = deviceName.getText();
		final String projectNameString = projectName.getText();
		final String addressString = address.getText();

		gotoGalleryScreen();

		getUiDevice().pressMenu();
		ClickByAndWait.text("Start a Report", "Opening reports");

		ClickByAndWait.text("T1 - Basic Features", "Opening T1 - Basic Features");

		final UiScrollable reportScroll = new UiScrollable(
				new UiSelector().description("custom_report_chapter_content"));

		// Checking PROJECT_INFO Question

		CheckIfUiObjectExistsBy.text("PROJECT_INFO Question",
				"Checking PROJECT_INFO question");

		for (int i = 0; projectNameString == "Unknown"; i++) {
			if (i >= 60)
				fail("timeout");
			Thread.sleep(1000);
		}

		final UiObject moreOptionsButton = new UiObject(new UiSelector().description("More options"));

		CheckIfUiObjectExistsBy.text(projectNameString,
				"Checking Project name value");

		CheckIfUiObjectExistsBy.text("Address", "Checking Address question");

		CheckIfUiObjectExistsBy.text(addressString, "Checking Address value");

		CheckIfUiObjectExistsBy.text("Device name",
				"Checking Device name question");

		CheckIfUiObjectExistsBy.text(deviceNameString,
				"Checking Device name value");

		// Checking TEXT question

		CheckIfUiObjectExistsBy.text("TEXT", "Checking TEXT question");

		CustomReportAnswers.inputTextAnswer("c1-s1-q2-t1", getUiDevice());

		// Checking TEXT_RICH question

		CheckIfUiObjectExistsBy.text("TEXT_RICH", "Checking TEXT_RICH question");

		CustomReportAnswers.inputTextAnswer("c1-s1-q3-t1", getUiDevice());

		CustomReportAnswers.testRichButtons("c1-s1-q3-r2", "c1-s1-q3-com1", "c1-s1-q3-r1", "TEXT_RICH", "Gallery",
				projectNameString,
				addressString, getUiDevice());

		// Checking PHOTO question

		reportScroll.scrollTextIntoView("PHOTOS");

		final UiObject photosButton = CheckIfUiObjectExistsBy.description(
				"c1-s1-q4-b1", "Checking PHOTOS button");
		CustomReportUtil.checkCustomReportButton(photosButton, "Add a Photo");
		photosButton.click();
		HelpingHand.getPhotoFromCamera(photosButton, getUiDevice());
		ClickByAndWait.description("Shutter", "Clicking Shutter");
		ClickByAndWait.description("Done", "Clikcing Done");
		ClickByAndWait.description("Select Photo", "Selecting Photo");

		// Checking FLIP_SWITCH question

		reportScroll.scrollTextIntoView("FLIP_SWITCH");

		final UiObject flipSwitchButttonYes = CheckIfUiObjectExistsBy.description(
				"c1-s1-q5-b1", "Checking FLIP_SWITCH button Yes");
		CustomReportUtil.checkFlipSwitchButton(flipSwitchButttonYes, "Yes", 0);

		final UiObject flipSwitchButtonNo = CheckIfUiObjectExistsBy.description(
				"c1-s1-q5-b1", "Checking FLIP_SWITCH button No");
		CustomReportUtil.checkFlipSwitchButton(flipSwitchButtonNo, "No", 0);

		System.out.println("Clicking Yes and checking properties");
		flipSwitchButttonYes.click();
		CustomReportUtil.checkFlipSwitchButton(flipSwitchButttonYes, "Yes", 1);

		// Checking FLIP_SWITCH_RICH question

		reportScroll.scrollTextIntoView("FLIP_SWITCH_RICH");

		final UiObject flipSwitchRichButttonYes = CheckIfUiObjectExistsBy.description(
				"c1-s1-q6-b1", "Checking FLIP_SWITC_RICH button Yes");

		CustomReportUtil.checkFlipSwitchButton(flipSwitchRichButttonYes, "Yes", 0);

		final UiObject flipSwitchRichButttonNo = CheckIfUiObjectExistsBy.description(
				"c1-s1-q6-b2", "Checking FLIP_SWITC_RICH button No");

		CustomReportUtil.checkFlipSwitchButton(flipSwitchRichButttonNo, "No", 0);

		System.out.println("Clicking No and checking properties");
		flipSwitchRichButttonNo.click();
		CustomReportUtil.checkFlipSwitchButton(flipSwitchRichButttonNo, "No", 1);

		CustomReportAnswers.testRichButtons("c1-s1-q6-r2", "c1-s1-q6-com1", "c1-s1-q6-r1", "FLIP_SWITC_RICH",
				"Gallery", projectNameString, addressString, getUiDevice());

		// Checking LIST_SINGLE_CHOICE question

		reportScroll.scrollTextIntoView("LIST_SINGLE_CHOICE_RICH");

		CheckIfUiObjectExistsBy.text("LIST_SINGLE_CHOICE",
				"Checking LIST_SINGLE_CHOICE question");

		final UiObject listSingleChoiceButton = CheckIfUiObjectExistsBy.description(
				"c1-s1-q7-b1", "Checking LIST_SINGLE_CHOICE button");
		CustomReportUtil.checkCustomReportButton(listSingleChoiceButton, "Select");
		listSingleChoiceButton.click();

		CheckIfUiObjectExistsBy.text("LIST_SINGLE_CHOICE",
				"Checking single select window title");

		// Checking single choices
		final String sct = CustomReportAnswers.singleChoiceTest("c1-s1-q7-b1"); // sct = single
		// choice text

		reportScroll.scrollTextIntoView("LIST_SINGLE_CHOICE_RICH");

		// Checking LIST_SINGLE_CHOICE_RICH question

		CheckIfUiObjectExistsBy.text("LIST_SINGLE_CHOICE_RICH",
				"Checking LIST_SINGLE_CHOICE_RICH question");

		final UiObject listSingleRichButton = CheckIfUiObjectExistsBy.description("c1-s1-q8-b1",
				"Checking LIST_SINGLE_CHOICE_RICH button");
		CustomReportUtil.checkCustomReportButton(listSingleRichButton, "Select");

		listSingleRichButton.clickAndWaitForNewWindow();

		CheckIfUiObjectExistsBy.text("LIST_SINGLE_CHOICE_RICH",
				"Checnkig LIST_SINGLE_CHOICE_RICH window title");

		final String scrt = CustomReportAnswers.singleChoiceTest("c1-s1-q8-b1"); // scrt = single choice rich text

		CustomReportAnswers.testRichButtons("c1-s1-q8-r2", "c1-s1-q8-com1", "c1-s1-q8-r1", "LIST_SINGLE_CHOICE_RICH",
				"Gallery",
				projectNameString, addressString, getUiDevice());

		reportScroll.scrollTextIntoView("DOUBLE_LIST_SINGLE_CHOICE_RICH");

		// Checking LIST_MULTI_CHOICE question

		CheckIfUiObjectExistsBy.text("LIST_MULTI_CHOICE", "Checking LIST_MULTI_CHOICE question");
		final UiObject listMultiButton = CheckIfUiObjectExistsBy.description("c1-s1-q9-b1",
				"Checking LIST_MULTI_CHOICE button");
		CustomReportUtil.checkCustomReportButton(listMultiButton, "Select");
		listMultiButton.clickAndWaitForNewWindow();

		CheckIfUiObjectExistsBy.text("LIST_MULTI_CHOICE", "Checking LIST_MULTI_CHOICE wndow title");

		final String mct = CustomReportAnswers.multiChoiceTest(listMultiButton); // mct = multi choice text

		// Checking LIST_MULTI_CHOICE_RICH question
		reportScroll.scrollTextIntoView("DOUBLE_LIST_SINGLE_CHOICE_RICH");

		final UiObject listMultiRichButton = CheckIfUiObjectExistsBy.description("c1-s1-q10-b1",
				"Checking LIST_MULTI_CHOICE_RICH button");
		CustomReportUtil.checkCustomReportButton(listMultiRichButton, "Select");
		listMultiRichButton.clickAndWaitForNewWindow();

		final String mcrt = CustomReportAnswers.multiChoiceTest(listMultiRichButton); // multi choice rich text

		CustomReportAnswers.testRichButtons("c1-s1-q10-r2", "c1-s1-q10-com1", "c1-s1-q10-r1", "LIST_MULTI_CHOICE",
				"Gallery",
				projectNameString, addressString, getUiDevice());

		reportScroll.scrollTextIntoView("LIST_SINGLE_CHOICE - With Numbers label question");

		// Checking DOUBLE_LIST_SINGLE_CHOICE_RICH question
		CheckIfUiObjectExistsBy.text("DOUBLE_LIST_SINGLE_CHOICE_RICH",
				"Checking DOUBLE_LIST_SINGLE_CHOICE_RICH question");
		UiObject doubleRichButton1 = CheckIfUiObjectExistsBy.description("c1-s1-q11-b1",
				"Checking DOUBLE_LIST_SINGLE_CHOICE_RICH Button #1");
		CustomReportUtil.checkCustomReportButton(doubleRichButton1, "Menu #1");
		doubleRichButton1.clickAndWaitForNewWindow();

		CheckIfUiObjectExistsBy.text("DOUBLE_LIST_SINGLE_CHOICE_RICH",
				"Checking DOUBLE_LIST_SINGLE_CHOICE_RICH wndow title");

		final UiObject dobleMenu1optionYes = CheckIfUiObjectExistsBy.text("Yes", "Checking Menu #1, option Yes");
		CustomReportUtil.checkSingleChoiceButtons(dobleMenu1optionYes, 0);

		final UiObject dobleMenu1optionNo = CheckIfUiObjectExistsBy.text("No", "Checking Menu #1, option No");
		CustomReportUtil.checkSingleChoiceButtons(dobleMenu1optionNo, 0);

		final UiObject option1 = new UiObject(new UiSelector().resourceId("android:id/text1").instance(
				HelpingHand.randomNumber(0, 1)));
		final String option1Text = option1.getText();
		option1.click();
		CustomReportUtil.checkSingleChoiceButtons(option1, 1);

		ClickByAndWait.text("OK", "Clicking OK");

		doubleRichButton1 = new UiObject(new UiSelector().description("c1-s1-q11-b1"));

		System.out.println("Checking new label for Menu #1");
		doubleRichButton1.getText().equals(option1Text);

		UiObject doubleRichButton2 = CheckIfUiObjectExistsBy.description("c1-s1-q11-b2",
				"Checking DOUBLE_LIST_SINGLE_CHOICE_RICH Button #2");
		CustomReportUtil.checkCustomReportButton(doubleRichButton2, "Menu #2");
		doubleRichButton2.clickAndWaitForNewWindow();

		CheckIfUiObjectExistsBy.text("DOUBLE_LIST_SINGLE_CHOICE_RICH",
				"Checking DOUBLE_LIST_SINGLE_CHOICE_RICH wndow title");

		final UiObject doubleMenu2OptionRed = CheckIfUiObjectExistsBy.text("Red", "Checking option Red");
		CustomReportUtil.checkSingleChoiceButtons(doubleMenu2OptionRed, 0);

		final UiObject doubleMenu2OptionGreen = CheckIfUiObjectExistsBy.text("Green", "Checking option Green");
		CustomReportUtil.checkSingleChoiceButtons(doubleMenu2OptionGreen, 0);

		final UiObject doubleMenu2OptionBlue = CheckIfUiObjectExistsBy.text("Blue", "Checking option Blue");
		CustomReportUtil.checkSingleChoiceButtons(doubleMenu2OptionBlue, 0);

		final UiObject doubleMenu2OptionOctarine = CheckIfUiObjectExistsBy.text("Octarine", "Checking Octarine");
		CustomReportUtil.checkSingleChoiceButtons(doubleMenu2OptionOctarine, 0);

		final UiObject option2 = new UiObject(new UiSelector().resourceId("android:id/text1").instance(
				HelpingHand.randomNumber(0, 3)));
		final String option2Text = option2.getText();
		option2.click();
		CustomReportUtil.checkSingleChoiceButtons(option2, 1);

		ClickByAndWait.text("OK", "Clicking OK");

		doubleRichButton2 = new UiObject(new UiSelector().description("c1-s1-q11-b2"));

		System.out.println("Checking new label for Menu #2");
		doubleRichButton2.getText().equals(option2Text);

		CustomReportAnswers.testRichButtons("c1-s1-q11-r2", "c1-s1-q11-com1", "c1-s1-q11-r1",
				"DOUBLE_LIST_SINGLE_CHOICE_RICH", "Gallery",
				projectNameString, addressString, getUiDevice());

		// Checking LIST_SINGLE_CHOICE - With Numbers label question
		CheckIfUiObjectExistsBy.text("LIST_SINGLE_CHOICE - With Numbers label",
				"Checking LIST_SINGLE_CHOICE - With Numbers label question");

		final UiObject listSingleCustomButton = CheckIfUiObjectExistsBy.description("c1-s1-q12-b1",
				"Checking LIST_SINGLE_CHOICE - With Numbers label button");

		CustomReportUtil.checkCustomReportButton(listSingleCustomButton, "Numbers");

		listSingleCustomButton.click();

		CheckIfUiObjectExistsBy.text("LIST_SINGLE_CHOICE - With Numbers label",
				"Checking LIST_SINGLE_CHOICE - With Numbers label window title");

		final String scclt = CustomReportAnswers.singleChoiceTest("c1-s1-q12-b1"); // scclt = single choice customs label text

		reportScroll.scrollTextIntoView("LIST_MULTI_CHOICE - with Numbers label question");

		// Checking LIST_SINGLE_CHOICE_RICH - With Numbers label question
		CheckIfUiObjectExistsBy.text("LIST_SINGLE_CHOICE_RICH - With Numbers label",
				"Checking LIST_SINGLE_CHOICE_RICH - With Numbers label question");

		final UiObject listSingleCustomRichButton = CheckIfUiObjectExistsBy.description("c1-s1-q13-b1",
				"Checking LIST_SINGLE_CHOICE_RICH - With Numbers label button");
		CustomReportUtil.checkCustomReportButton(listSingleCustomRichButton, "Numbers");

		listSingleCustomRichButton.click();

		CheckIfUiObjectExistsBy.text("LIST_SINGLE_CHOICE_RICH - With Numbers label",
				"Checking LIST_SINGLE_CHOICE_RICH - With Numbers label window title");

		final String scrclt = CustomReportAnswers.singleChoiceTest("c1-s1-q13-b1"); // scrclt = single choice rich custom label text

		CustomReportAnswers.testRichButtons("c1-s1-q13-r2", "c1-s1-q13-com1", "c1-s1-q13-r1",
				"LIST_SINGLE_CHOICE_RICH - With Numbers label", "Camera", projectNameString, addressString,
				getUiDevice());

		// Checking LIST_MULTI_CHOICE - with Numbers label question

		reportScroll.scrollToEnd(3);

		CheckIfUiObjectExistsBy.text("LIST_MULTI_CHOICE - with Numbers label",
				"Checking LIST_MULTI_CHOICE - with Numbers label question");

		final UiObject listMultiCustomButton = CheckIfUiObjectExistsBy.description("c1-s1-q14-b1",
				"Checking LIST_MULTI_CHOICE - With Numbers label button");
		CustomReportUtil.checkCustomReportButton(listMultiCustomButton, "Numbers");

		listMultiCustomButton.click();

		CheckIfUiObjectExistsBy.text("LIST_MULTI_CHOICE - with Numbers label",
				"Checking LIST_MULTI_CHOICE - with Numbers label window title");

		final String mcclt = CustomReportAnswers.multiChoiceTest(listMultiCustomButton); // mcclt = multi choice custom label text

		// Checking LIST_MULTI_CHOICE_RICH - with Numbers label question
		reportScroll.scrollToEnd(3);

		CheckIfUiObjectExistsBy.text("LIST_MULTI_CHOICE_RICH - with Numbers label",
				"Checking LIST_MULTI_CHOICE_RICH - with Numbers label question");

		final UiObject listMultiRichCustomButton = CheckIfUiObjectExistsBy.description("c1-s1-q15-b1",
				"Checking LIST_MULTI_CHOICE_RICH - with Numbers label button");
		CustomReportUtil.checkCustomReportButton(listMultiRichCustomButton, "Numbers");
		listMultiRichCustomButton.clickAndWaitForNewWindow();

		CheckIfUiObjectExistsBy.text("LIST_MULTI_CHOICE_RICH - with Numbers label",
				"Checking LIST_MULTI_CHOICE_RICH - with Numbers label window title");

		final String mcrclt = CustomReportAnswers.multiChoiceTest(listMultiRichCustomButton); // mcrclt = multi choice rich custom label text

		CustomReportAnswers.testRichButtons("c1-s1-q15-r2", "c1-s1-q15-com1", "c1-s1-q15-r1",
				"LIST_MULTI_CHOICE_RICH - with Numbers label", "Gallery", projectNameString, addressString,
				getUiDevice());

		System.out.println("C4581	2.4.1. Master Testing Report - T1 - All Questions  Passed");

		Thread.sleep(500);

		// Testing C4582 2.4.2. Master Testing Report - T1 - Data Persistence

		// getUiDevice().click(150, 100);

		Screen.openDrawer(getUiDevice());
		Thread.sleep(350);
		Screen.closeReportDrawer();

		ClickByAndWait.description("Test Chapter, Navigate up", "Returning to reports menu");

		UiObject basicReport = new UiObject(new UiSelector().text("Basic report"));

		for (int i = 0; !basicReport.exists(); i++) {
			if (i >= 60)
				fail("timeout");

			ClickByAndWait.description("Test Chapter, Navigate up", "Returning to reports menu");

			Thread.sleep(200);

			basicReport = new UiObject(new UiSelector().text("Basic report"));

			Thread.sleep(800);
		}

		ClickByAndWait.text("T1 - Basic Features", "Opening T1 - Basic Features");

		CheckIfUiObjectExistsBy.text("Novar", "Checking Project name");

		CheckIfUiObjectExistsBy.text("Firmat", "Checking Address");

		CustomReportUtil.checkReportData("c1-s1-q2-t1", "c1-s1-q2-t1", "Text question");

		CustomReportUtil.checkReportData("c1-s1-q3-t1", "c1-s1-q3-t1", "Text Rich question");

		CustomReportUtil.checkReportData("c1-s1-q3-t1", "c1-s1-q3-r2", "Text Rich Comment");

		reportScroll.scrollTextIntoView("FLIP_SWITCH");

		final UiObject flipSwitchYes = new UiObject(new UiSelector().description("c1-s1-q5-b1"));
		CustomReportUtil.checkFlipSwitchButton(flipSwitchYes, "Yes", 1);

		final UiObject flipSwitchNo = new UiObject(new UiSelector().description("c1-s1-q5-b2"));
		CustomReportUtil.checkFlipSwitchButton(flipSwitchNo, "No", 0);

		final UiObject flipSwithcRichYes = new UiObject(new UiSelector().description("c1-s1-q6-b1"));
		CustomReportUtil.checkFlipSwitchButton(flipSwithcRichYes, "Yes", 0);

		final UiObject flipSwitchRichNo = new UiObject(new UiSelector().description("c1-s1-q6-b2"));
		CustomReportUtil.checkFlipSwitchButton(flipSwitchRichNo, "No", 1);

		CustomReportUtil.checkReportData("c1-s1-q6-t1", "c1-s1-q6-r2", "Flip Switch Rich Comment");

		CustomReportUtil.checkReportData("c1-s1-q7-b1", sct, "List Single Choice button");

		CustomReportUtil.checkReportData("c1-s1-q8-b1", scrt, "List Single Choice Rich button");

		CustomReportUtil.checkReportData("c1-s1-q8-t1", "c1-s1-q8-r2", "List Single Choice Rich Comment");

		CustomReportUtil.checkReportData("c1-s1-q9-b1", mct, "Multi Choice button");

		CustomReportUtil.checkReportData("c1-s1-q10-b1", mcrt, "Multi Choice Rich button");

		CustomReportUtil.checkReportData("c1-s1-q10-t1", "c1-s1-q10-r2", "Multi Choice Rich comment");

		CustomReportUtil.checkReportData("c1-s1-q11-b1", option1Text, "Double Multi Choice Rich Button 1");

		CustomReportUtil.checkReportData("c1-s1-q11-b2", option2Text, "Double Multi Choice Rich Button 2");

		CustomReportUtil.checkReportData("c1-s1-q11-t1", "c1-s1-q11-r2", "Double Multi Choice Comment");

		CustomReportUtil.checkReportData("c1-s1-q12-b1", scclt, "List Single Choice - custom label button");

		CustomReportUtil.checkReportData("c1-s1-q13-b1", scrclt, "List Single Choice Rich - custom label text");

		CustomReportUtil.checkReportData("c1-s1-q13-t1", "c1-s1-q13-r2", "List Single Choice Rich - custom label comment");

		CustomReportUtil.checkReportData("c1-s1-q14-b1", mcclt, "Multi Choice - custom label - custom label text");

		CustomReportUtil.checkReportData("c1-s1-q15-b1", mcrclt, "Multi Choice Rich - custom label - custom label text");

		CustomReportUtil.checkReportData("c1-s1-q15-t1", "c1-s1-q15-r2",
				"Multi Choice Rich - custom label - custom label comment");

		System.out.println("C4582 2.4.2. Master Testing Report - T1 - Data Persistence passed");

		// C4580 2.4. Master Testing Report - T1 - Render the report

		CustomReportUtil.uploadPDF(getUiDevice());

		System.out.println("C4580 2.4. Master Testing Report - T1 - Render the report passed");
		System.out.println("Check fotoin.egnyte.com, Shared/uitesting/reports/date for PDF");

		// C7119 2.4.4. MasterTesting Report - T1 - Reset the report

		moreOptionsButton.click();

		ClickByAndWait.text("Reset", "Reseting report");

		System.out.println("Checking Project name");

		UiObject projectNameValue = new UiObject(new UiSelector().description("c1-s1-q1-t1"));

		for (int i = 0; !projectNameValue.getText().equals(projectNameString); i++) {
			if (i >= 60)
				fail("timeout");

			getUiDevice().pressBack();
			Thread.sleep(300);
			ClickByAndWait.text("T1 - Basic Features", "Opening T1 - Basic Features");

			projectNameValue = new UiObject(new UiSelector().description("c1-s1-q1-t1"));
			Thread.sleep(700);

		}

		CustomReportUtil.checkReportData("c1-s1-q1-t2", addressString, "Address text");

		CustomReportUtil.checkReportData("c1-s1-q1-t3", deviceNameString, "Device name text");

		CustomReportUtil.checkReportData("c1-s1-q2-t1", defaultTextValue, "TEXT text");

		CustomReportUtil.checkReportData("c1-s1-q3-t1", defaultTextValue, "TEXT RICH text");

		CustomReportUtil.checkFlipSwitchButton(flipSwitchYes, "Yes", 0);

		CustomReportUtil.checkFlipSwitchButton(flipSwitchNo, "No", 0);

		CustomReportUtil.checkFlipSwitchButton(flipSwithcRichYes, "Yes", 0);

		CustomReportUtil.checkFlipSwitchButton(flipSwitchRichNo, "No", 0);

		CustomReportUtil.checkReportData("c1-s1-q7-b1", "Select", "LIST_SINGLE_CHOICE button");

		CustomReportUtil.checkReportData("c1-s1-q8-b1", "Select", "LIST_SINGLE_CHOICE_RICH button");

		CustomReportUtil.checkReportData("c1-s1-q9-b1", "Select", "LIST_MULTI_CHOICE button");

		CustomReportUtil.checkReportData("c1-s1-q10-b1", "Select", "LIST_MULTI_CHOICE_RICH button");

		CustomReportUtil.checkReportData("c1-s1-q11-b1", "Menu #1", "DOUBLE_LIST_SINGLE_CHOICE_RICH Button 1");

		CustomReportUtil.checkReportData("c1-s1-q11-b2", "Menu #2", "DOUBLE_LIST_SINGLE_CHOICE_RICH Button 2");

		CustomReportUtil.checkReportData("c1-s1-q12-b1", "Numbers", "LIST_SINGLE_CHOICE - With Numbers label button");

		CustomReportUtil.checkReportData("c1-s1-q13-b1", "Numbers", "LIST_SINGLE_CHOICE_RICH - With Numbers label button");

		CustomReportUtil.checkReportData("c1-s1-q14-b1", "Numbers", "LIST_MULTI_CHOICE - with Numbers label button");

		CustomReportUtil.checkReportData("c1-s1-q15-b1", "Numbers", "LIST_MULTI_CHOICE_RICH - with Numbers label button");

		System.out.println("C7119 2.4.4. MasterTesting Report - T1 - Reset the report passed");
	}

}
