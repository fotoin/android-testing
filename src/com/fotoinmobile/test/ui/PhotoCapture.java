package com.fotoinmobile.test.ui;

import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiSelector;
import com.fotoinmobile.test.ui.util.AppStart;
import com.fotoinmobile.test.ui.util.ClickByAndWait;
import com.fotoinmobile.test.ui.util.FotoINBaseTest;
import com.fotoinmobile.test.ui.util.HelpingHand;
import com.fotoinmobile.test.ui.util.Screen;

public class PhotoCapture extends FotoINBaseTest {

	public void testPhotoCapture() throws Exception {

		/*
		 * C4539 1.1.1. Photo Capture from Photo belonging to someone else
		 */

		unlockScreen();
		AppStart.slideToUnlock();
		clearData();
		AppStart.startFotoIN(getUiDevice());
		AppStart.skipGuide();
		AppStart.autoSyncOnOff(0);
		companyLogin("uitesting", "123456");
		gotoGalleryScreen();
		sync();

		UiObject listView = new UiObject(new UiSelector().description("gallery_photo_list"));
		int numberOfPhotos = listView.getChildCount();

		ClickByAndWait.resourceId("com.fotoinmobile.construction:id/row_image", "Opening photo from galery");

		HelpingHand.snapPhoto();

		Screen.openDrawer(getUiDevice());

		ClickByAndWait.text("Edit", "Clicking Edit button");

		UiObject projectValue = new UiObject(new UiSelector().description("Project_value"));
		String project = projectValue.getText();

		UiObject addressValue = new UiObject(new UiSelector().description("Address_value"));
		String address = addressValue.getText();

		final UiObject refreshAddressButton = new UiObject(new UiSelector().description("Find adresss_button"));

		while (project.equals("Unknown") || address.equals("Unknown")) {
			refreshAddressButton.click();

			Thread.sleep(200);

			projectValue = new UiObject(new UiSelector().description("Project_value"));
			project = projectValue.getText();

			addressValue = new UiObject(new UiSelector().description("Address_value"));
			address = addressValue.getText();

			System.out.println("Project name is " + project + ", address is " + address);
		}

		ClickByAndWait.description("FotoIN, Navigate up", "Returning to gallery");

		listView = new UiObject(new UiSelector().description("gallery_photo_list"));

		if (numberOfPhotos < listView.getChildCount())
			System.out.println("New photo saved to gallery");
		else
			fail("Photo not saved to gallery!");
	}

}
